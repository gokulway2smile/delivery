(function() {
    'use strict';
    angular.module('app').controller('DashboardCtrl', ['$rootScope','$scope', '$state', '$mdEditDialog', '$q', '$timeout', '$http', '$mdDialog', 'api', DashboardCtrl])

    function DashboardCtrl($rootScope,$scope, $state, $mdEditDialog, $q, $timeout, $http, $mdDialog, api) {
        function dataCtrl($rootScope,$scope, $mdDialog, dataToPass, api,$window, $mdSelect,$timeout) {
                
                $http.get('gettimezone/').then(function (response){
                $scope.timeZone=angular.fromJson(response).data;
            });
                $rootScope.updatebasicinfo = false;
                $scope.timezone=$rootScope.timeData.timezone;
                if($rootScope.timeData.mailnote == 1){
                    $scope.mail=1;
                    $scope.mailsts=true;

                }
                if($rootScope.timeData.smsnote == 1){
                    $scope.sms=1;
                    $scope.smssts=true;
                }
                 if($rootScope.timeData.is_multipick == 1){
                    $scope.is_multipick=1;
                }
                $scope.address=$rootScope.timeData.street;
                $scope.cancel = function() {
                    if($rootScope.timeData.timeZone && $rootScope.timeData.street){
                        $mdDialog.cancel();
                    }
            };
                $scope.pickup_ladd = 0;
                $scope.pickup_long = 0;

            $scope.geocodeupdatelocpickup = function() {
                var address = $scope.address;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            console.log(results);
                            $scope.getpickuppos(location);
                        } else {}
                    });
                }
            }
             $scope.getpickuppos = function(data, zoom,$status) {
                var zoom = zoom || 18;
                console.log(data);


$scope.map = {
      center: {
        latitude: 21.0000,
        longitude: 78.0000
      },
      zoom: 4,
      events: {
        click: function(mapModel, eventName, originalEventArgs,ok) {
          var e = originalEventArgs[0]; 

            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {

                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });


        }
      }
    };



                $scope.pickup_ladd = data.lat();
                $scope.pickup_long = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
                        $scope.showpickupmap = true;

            };
                            $scope.geocodeupdatelocpickup();

 $scope.searchTextChange=function(text) {
      $log.info('Text changed to ' + text);
    }

    $scope.selectedItemChange=function(item) {
        console.log(item)
      $log.info('Item changed to ' + JSON.stringify(item));
    }

$scope.querySearch =function(query) {
       var deferred = $q.defer();
           
                var states = $scope.timeZone.filter(function(state) {
                   
                    return (state.desc.toUpperCase().indexOf(query.toUpperCase()) !== -1 || state.desc.toUpperCase().indexOf(query.toUpperCase()) !== -1);
                });

                deferred.resolve(states);
                console.log(deferred.promise);
            return deferred.promise;
    }

        $scope.submit_data = function(){
            console.log(this.ctrl.searchText);

            if(!$scope.mail){
                $scope.mail = 0;
            }
            if(!$scope.sms){
                $scope.sms = 0;
            }
            if(!$scope.is_multipick){
                $scope.is_multipick = 0;
            }
            if ($scope.pickup_ladd == 0 || $scope.pickup_long == 0) {
                    return;
                }
            $http.post('updatebasicinfo/',{data:{"timezone":this.ctrl.searchText,
                "mailnote":$scope.mail,
                "smsnote":$scope.sms,
                "street":$scope.address,
                "is_multipick":$scope.is_multipick
             }
            }).then(function(response){
                          $mdDialog.show($mdDialog.alert().title("This TimeZone is Used For Your Applications").ok('OK'));
                                      var res = angular.fromJson(response);
                                      $scope.data = res.data.data;

                $rootScope.timeData=$scope.data;
                // debugger;
                console.log($rootScope.timeData);
                window.location.reload();

            });

        }

        }

        function DialogController($rootScope,$scope, $mdDialog, dataToPass, api,$window, $mdSelect,$timeout) {

            //  $window.addEventListener('click',function(e){

            //     console.log(e);

            //  $mdSelect.hide();
            // })
            console.log("data");
             if($rootScope.timeData.is_multipick == 1){
                    $scope.sts = true;
                }else{
                    $scope.sts = false;
                }
                           $scope.records = [];
                           $scope.multipickup = false;

            $scope.isSubscribed=true;
            $scope.list=[]; 
            $scope.countval=0;
            $scope.tripstart=false;
            localStorage.removeItem("added-cust");
            $scope.passed = dataToPass;
            if (!dataToPass.data) {
                $scope.data = {};
                $scope.add = true;
                $scope.data.status = 'Unallocated';
                $scope.data.type = 0;
                $scope.data.method = 'Pickup';
                $scope.datePicker = new Date();
                $scope.data.picktime= new Date();
                $scope.data.loc_lat = 0;
                $scope.data.loc_lng = 0;
                 $scope.data.pickup_ladd = 0;
                $scope.data.pickup_long = 0;
                $scope.data.sent_ladd = 0;
                $scope.data.sent_long = 0;
                $scope.edit = true;
            } else {
                $scope.edit = false;
                $scope.data = dataToPass.data;
                $scope.data.type = 0;
                $scope.add = false;
                $scope.datePicker = moment($scope.data.schedule_date_time)._d;
                if ($scope.data.allocated_emp_id) {
                    $scope.data.emp = $scope.data.allocated_emp_id;
                }
            }


            $scope.edit = true;


            if($scope.passed.dataView)
            {

                    $scope.edit = false;
                    $scope.data = dataToPass.data;
            }

               
            $scope.apipath = 'new_order/';
            $scope.temp = api.users();
            $scope.temp().then().then(function(data) {
                $scope.users = data.data;
            });
            $scope.alert = '';
            $scope.showmap = false;
            $scope.mapzoom = 18;
            $scope.customers = [];
            $scope.schedule = api.schedule;
            $scope.tempcus = api.customers();
            $scope.place_type = api.place_type;
            $scope.pickups=api.pickup();
            $scope.method = api.method;
            $scope.email_rex = api.email_rex;
            $scope.web_rex = api.web_rex;
            $scope.tempcus().then().then(function(data) {
                $scope.customers = data.data;
            });
            $scope.pickups().then().then(function(data) {
                $scope.pickup = data.data;
            });
            $scope.dateCheck = function() {
                if ($scope.datePicker._i.length == 13) {
                    console.log('Date only');
                }
            }
            function callAtTimeout() {
    console.log("Timeout occurred");
}
            $scope.addtolist=function(){
                if(!$scope.add){
                    $scope.records = $scope.data.records;
                   // console.log($scope.data.records);
                var pictime=moment($scope.data.picktime).format("YYYY-MM-DD, h:mm A")
                var data = {
                    pic_loc: $scope.data.pick_address,
                    pic_date: pictime,
                    pic_item: $scope.data.notes,
                    pic_ladd : $scope.data.pickup_ladd,
                    pic_long : $scope.data.pickup_long
                };
                 $scope.records.push(data);
                 $scope.data.pick_address = '';
                 $scope.data.notes = '';
                 $scope.data.records = $scope.records;
                }else{
                  var pictime=moment($scope.data.picktime).format("YYYY-MM-DD, h:mm A")
                  var data = {
                    pic_loc: $scope.data.pick_address,
                    pic_date: pictime,
                    pic_item: $scope.data.notes,
                    pic_long : $scope.data.pickup_ladd,
                    pic_ladd : $scope.data.pickup_long
                };
                 $scope.records.push(data);
                 $scope.data.pick_address = '';
                 $scope.data.notes = '';
                $scope.data.records = $scope.records;
                }
                $scope.data.pickup_ladd
            }
            $scope.editpickup = function(index){
                
                var datalist = $scope.data.records[index];
                $scope.data.pick_address = datalist.pic_loc;
                $scope.data.notes = datalist.pic_item; 
                $scope.data.pickup_ladd = datalist.pic_ladd;
                $scope.data.pickup_long = datalist.pic_long; 
                 $scope.data.records.splice(index,1);

            }
            $scope.deletepickup = function(index){
                $scope.data.records.splice(index,1);

            }
          $scope.datadesel = function(){
            if($scope.is_geo_fence==1){
                            $scope.is_geo_fence=0;
            }else{
                            $scope.is_geo_fence=1;
            }
        }
            $scope.submit_form_multicustomer=function(){
                 console.log($scope.datePicker);
                   var fd = new FormData();
                    fd.append('data', $scope.list); 
                    fd.append('schedule_date_time', api.CovertDateTime($scope.datePicker)); 
                    console.log(fd);
                    $http.post('multidelivery', {
                    data: $scope.list,

                    }).then(function(response) {
                        var res = angular.fromJson(response);
                        if (res.data.status == 'ok') {
                            $mdDialog.hide('reload');
                            $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                            // setTimeout($scope.cancel(), 2000)
                        } else {
                            console.log(res);
                            //$scope.alert = res.data.data;
                        }
                    }, function(response) {
                       console.log('err1');
                        // failure call back
                    });
            }
            $scope.customersChange = function() {
                var   x=angular.element(document.getElementById("add-dash-phoneno"));      
                $scope.phone = x.val();
                var hasMatch =false;
                for (var index = 0; index < $scope.customers.length; ++index) {
                 if($scope.customers[index].contact_no == $scope.phone){
                   hasMatch = true;
                   break;
                 }
                }
                if(hasMatch){
                var choosenCustomer = $scope.customers[index];
                if (choosenCustomer) {
                    $scope.data.cust_phone = choosenCustomer.contact_no;
                    $scope.data.cust_email = choosenCustomer.email;
                    $scope.data.cust_name = choosenCustomer.name;
                    $scope.data.cust_address = choosenCustomer.address;
                    $scope.data.loc_lat = choosenCustomer.loc_lat;
                    $scope.data.loc_lng = choosenCustomer.loc_lng;
                }
            }
            }
            $scope.emptyCustomerInfo = function(id) {
                $scope.data.cust_phone = '';
                $scope.data.cust_email = '';
                $scope.data.cust_name = '';
                $scope.data.cust_address = '';
                $scope.data.loc_lat = 0;
                $scope.data.loc_lng = 0;
                $scope.data.cust_id = '';
            };
            $scope.geocodeupdateloc1 = function() {
                var address = $scope.data.cust_address;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            $scope.getpos(location);
                        } else {}
                    });
                }
            }
             $scope.geocodeupdatelocpickup = function() {
                var address = $scope.data.pick_address;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            console.log(results);
                            $scope.getpickuppos(location);
                        } else {}
                    });
                }
            }
            $scope.geocodeupdatelocsender = function() {
                var address = $scope.data.sent_address;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            $scope.getsenderpos(location);
                        } else {}
                    });
                }
            }
            $scope.getsenderpos = function(data, zoom,status) {
                var zoom = zoom || 18;
                 if(status){
                  var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(data.lat(), data.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            $scope.data.sent_address= results[1].formatted_address;
                            $scope.data.sent_ladd = data.lat();
                            $scope.data.sent_long = data.lng();
                            $scope.mapzoom = zoom;
                            $scope.$apply();
                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
        }
                $scope.data.sent_ladd = data.lat();
                $scope.data.sent_long = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            };
            
             $scope.getpickuppos = function(data, zoom,status) {
                
                var zoom = zoom || 18;
                console.log(data);
                if(status){
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(data.lat(), data.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            $scope.data.pick_address= results[1].formatted_address;
                            $scope.data.pickup_ladd = data.lat();
                            $scope.data.pickup_long = data.lng();
                            $scope.mapzoom = zoom;
                            $scope.$apply();
                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
                console.log($scope.data.pick_address);
            }
                $scope.data.pickup_ladd = data.lat();
                $scope.data.pickup_long = data.lng();
                $scope.mapzoom = zoom;
                
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            };
 
             $scope.setadd = function()
             {
                $scope.getpickuppos();
             };
            $scope.getpos = function(data, zoom,status) {
                var zoom = zoom || 18;
                 if(status){
                 var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(data.lat(), data.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            $scope.data.cust_address= results[1].formatted_address;
                            $scope.data.loc_lat = data.lat();
                            $scope.data.loc_lng = data.lng();
                            $scope.mapzoom = zoom;
                            $scope.$apply();
                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
        }
                $scope.data.loc_lat = data.lat();
                $scope.data.loc_lng = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            };
            $scope.submit_form = function() {
                if($scope.multipickup){
                    if($scope.add){
                    var arrayrecords = $scope.records;
                    }else{
                    var arrayrecords = $scope.data.records;
                    }
                    console.log(arrayrecords);
                    var i=0;
                    var pic_loc = [];
                    var pic_date = [];
                    var pic_item = [];
                    var pic_ladd = [];
                    var pic_long = [];
                    for(;i<arrayrecords.length;i++){
                        if(i == (arrayrecords.length)-1){
                            pic_loc [i] =arrayrecords[i].pic_loc
                        }else{
                            pic_loc [i] =arrayrecords[i].pic_loc+"||" 
                        }
                        pic_item [i]=arrayrecords[i].pic_item;
                        pic_ladd [i]=arrayrecords[i].pic_ladd;
                        pic_long [i]=arrayrecords[i].pic_long;
                    }                    
                  $scope.data.pick_address = pic_loc.toString(),
                  $scope.data.notes = pic_item.toString();
                  $scope.data.pickup_ladd = pic_long.toString();
                  $scope.data.pickup_long = pic_ladd.toString();
                  
                }else{

                }
                if ($scope.data_form1.$invalid) {
                    // if(!$scope.multipickup){

                    //     if(arrayrecords.length == 0){
                    //         $scope.alert = 'Please Add The Pickup Point To List ';
                    //         return;
                    //     }
                    // }else{
                    //                             alert(arrayrecords.length);

                    //         return;
                    // }
                }
                if ($scope.data.loc_lat == 0 || $scope.data.loc_lng == 0) {
                    return;
                }
                 else{
                    console.log('Address Lat Lang is Valid');
                }

                // if ($scope.datePicker._i.length == 13) {
                //                $scope.alert = 'Date time Invalid';
                // }


                console.log($scope.data.mob);
                $scope.data.schedule_date_time = api.CovertDateTime($scope.datePicker);
                $scope.data.pick_date_time = api.CovertDateTime($scope.data.picktime);
                console.log($scope.data.sent_ladd);
                console.log($scope.data.comments+$scope.data.mob);
                if ($scope.add == true) {
                    $http.post($scope.apipath, {
                        data: $scope.data,
                        comments:$scope.data.comments,
                        mob:$scope.data.mob,
                        cust_email:$scope.data.cust_email,
                        sender_name:$scope.data.sender_name,
                        sender_number:$scope.data.sender_number,
                        sent_ladd:$scope.data.sent_ladd ,
                        sent_long:$scope.data.sent_long ,
                    }).then(function(response) {
                        var res = angular.fromJson(response);
                        console.log(res);
                        if (res.data.status == 'ok') {
                            $mdDialog.hide('reload');
                            $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                            // setTimeout($scope.cancel(), 2000)
                        } else {
                          $scope.alert = res.data.data;
                        }
                    }, function(response) {
                        // failure call back
                    });
                } else {
                    $http.put($scope.apipath + $scope.data.id, {
                        data: $scope.data,
                         comments:$scope.data.comments,
                        mob:$scope.data.mob,
                        cust_email:$scope.data.cust_email,
                        sender_name:$scope.data.sender_name,
                        sender_number:$scope.data.sender_number
                    }).then(function(response) {
                        var res = angular.fromJson(response);
                        if (res.data.status == 'ok') {
                            //   $scope.alert = res.data.data;
                            $mdDialog.hide('reload');
                            $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                            //setTimeout($scope.cancel(), 2000)
                        } else {
                            $scope.alert = res.data.data;
                        }
                    }, function(response) {
                        // failure call back
                    });
                }
                console.log($scope.data);
            };
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
        }
        $scope.datadesel = function(){
           
        }
        $scope.showprofileupdate = function(ev){
             $http.get('profile').then(function(response) {
                        var res = angular.fromJson(response);
                        if (res.data.status == 'ok') {
                            $rootScope.timeData = res.data.data;
                            console.log($rootScope.timeData);
                             if($rootScope.timeData.street && $rootScope.timeData.timezone){
                                }
                             else{
                                console.log("no timezone");
                                console.log($rootScope.updatebasicinfo)
                                if($rootScope.updatebasicinfo){
                                  $http.get('gettimezone/').then(function (response){
                                    $scope.timeZone=angular.fromJson(response).data;
                                    var timezone = $scope.timeZone;
                                                  $mdDialog.show({
                                        controller: dataCtrl,
                                         locals: {
                                            dataToPass: timezone
                                        },
                                        templateUrl: 'assets/app/profileupdate/profileupd.html?' + Math.random(),
                                        targetEvent: ev,
                                        clickOutsideToClose: false
                                    });
                                    console.log(timezone);

                                });

                                    }
                                }
                         
                        } else { console.log("eror") }
                    },function(response) { }
            );
           
        }
       
        $scope.showTabDialog = function(ev, add, id) {
            var dat1 = new Date($rootScope.packData[0].end_date);
            var dat2 = new Date();
            if(dat1 < dat2){
                $mdDialog.show(
                                $mdDialog.alert()
                                .title('Your trial period has expired. You are not able to add drivers / tasks. Please contact us to upgrade your account.').ok('OK')
                            );
                return;
            }
            var add = add || true;
            if (add == true) {
                
                var data = false;
                $mdDialog.show({
                    controller: DialogController,
                    locals: {
                        dataToPass: data
                    },
                    templateUrl: 'assets/app/new-schedule/pickup_modal_view.html?' + Math.random(),
                    targetEvent: ev,
                    clickOutsideToClose: false
                }).then(function(answer) {
                    if (answer == 'reload') {
                        $scope.updateData();
                    }
                }, function() {});
                }
                else if (add == 11) {
                                var data = {
                                    'dataView' : true,
                                    'data' : id
                                };
                $mdDialog.show({
                    controller: DialogController,
                    locals: {
                        dataToPass: data
                    },
                    templateUrl: 'assets/app/new-schedule/pickup_modal_view.html?' + Math.random(),
                    targetEvent: ev,
                    clickOutsideToClose: false
                }).then(function(answer) {
                    if (answer == 'reload') {
                        $scope.updateData();
                    }
                }, function() {});

                }
            else {
                $http.get('new_task/' + id).then(function(data) {
                    var data = data.data;
                    console.log(data);
                    $scope.records = [];
                var addressfields = data.data.pick_address.split('||,');
                var notesfields = data.data.notes.split(',');
                var laddfields = data.data.pickup_ladd.split(',');
                var longfields = data.data.pickup_long.split(',');
                var i= 0;
                for(;i<addressfields.length;i++){
                    var newdata = {
                        pic_loc: addressfields[i],
                        pic_item: notesfields[i],
                        pic_ladd : laddfields[i],
                        pic_long : longfields[i]
                    };
                 $scope.records.push(newdata);

                }
                data.data.pick_address = newdata.pic_loc;
                data.data.notes = newdata.pic_item;
                data.data.records = $scope.records;
                   // console.log(data.data.status);
                    if(data.data.status == 'Started Ride' || data.data.status == 'In Supplier Place' || data.data.status == 'In Supplier Place' ){
                        data.data.tripstart = true;
                        console.log(data);
                    }
                    $mdDialog.show({
                        controller: DialogController,
                        locals: {
                            dataToPass: data,
                        },
                        templateUrl: 'assets/app/new-schedule/pickup_modal_view.html?' + Math.random(),
                        targetEvent: ev,
                        clickOutsideToClose: false
                    }).then(function(answer) {
                        if (answer == 'reload') {
                            $scope.updateData();
                        }
                    }, function() {});
                });
            }
        };
        $scope.customers = [];
        $scope.users = [];
        $scope.tasksData = {};
        $scope.tasksData.data = [];
        $scope.task = {};
        $scope.cust_id = '';
        $scope.task.driver = true;
        $scope.task.expanded = false;
        $scope.showTrafficLayer = false;
        $scope.taskFilter = '';
        $scope.taskFilterSt = false;
        $scope.DriverFilter = '';
        $scope.DriverFilterSt = false;
        $scope.RouteOrigin = api.mapcenter;
        $scope.SideBar = true;
        $scope.trackingMap = true;
        $scope.date = new Date();
        $scope.apipath = 'new_order/';
        $scope.driverDefaultImg = 'assets/images/canva-man-delivering-boxes-on-scooter-icon-MAB60VR-Abk.png';
        $scope.updateData = function() {
            $scope.tempcus = api.customers();
            $scope.tempcus().then().then(function(data) {
                $scope.customers = data.data;
            });
            $scope.tempusers = api.users();
            $scope.tempusers().then().then(function(data) {
                $scope.users = data.data;
            });
            var tepDate = moment($scope.date).format('YYYY-MM-DD');
            // console.log($rootScope.packData);
            if($rootScope.packData)
            {
            var dat1 = new Date($rootScope.packData[0].end_date);
            var dat2 = new Date();
            if(dat1 < dat2){
                return;
            }else{
            $http.get($scope.apipath + '?date=' + tepDate).then(function(response) {

                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    $scope.tasksData.data = res.data.data;
                    $http.get('deftimezone').then(function(response){
                         var responsedata = angular.fromJson(response).data;
                        //return res.data;
                        console.log(responsedata);
                         var picdates = [];
                    var j=0
                    var array=res.data.data;
                    for(;j<array.length;j++){
                    var date1 = new Date($scope.tasksData.data[j].picktime);
                    if(new Date(responsedata) > new Date(date1)){
                                            var status=$scope.tasksData.data[j].status;
                                              if(status == "Unallocated"){
                                                  picdates.push($scope.tasksData.data[j].id);
                                                  $scope.tasksData.data[j].style='border: solid 2px red';
                                              }
                                              else if(status == "Started Ride"){
                                                  picdates.push($scope.tasksData.data[j].id);
                                                  $scope.tasksData.data[j].style='border: solid 2px red';
                                              }
                                             else if(status == "Allocated"){
                                                  picdates.push($scope.tasksData.data[j].id);
                                                  $scope.tasksData.data[j].style='border: solid 2px red';
                                              }
                    }
                    
                    }

                    });
                                        
                } else {}
            }, function(response) {
                // failure call back
            });
        }
    }
    else
    {
        $http.get($scope.apipath + '?date=' + tepDate).then(function(response) {

                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    $scope.tasksData.data = res.data.data;
                    $http.get('deftimezone').then(function(response){
                         var responsedata = angular.fromJson(response).data;
                        //return res.data;
                        console.log(responsedata);
                         var picdates = [];
                    var j=0
                    var array=res.data.data;
                    for(;j<array.length;j++){
                    var date1 = new Date($scope.tasksData.data[j].picktime);
                    if(new Date(responsedata) > new Date(date1)){
                                            var status=$scope.tasksData.data[j].status;
                                              if(status == "Unallocated"){
                                                  picdates.push($scope.tasksData.data[j].id);
                                                  $scope.tasksData.data[j].style='border: solid 2px red';
                                              }
                                              else if(status == "Started Ride"){
                                                  picdates.push($scope.tasksData.data[j].id);
                                                  $scope.tasksData.data[j].style='border: solid 2px red';
                                              }
                                             else if(status == "Allocated"){
                                                  picdates.push($scope.tasksData.data[j].id);
                                                  $scope.tasksData.data[j].style='border: solid 2px red';
                                              }
                    }
                    
                    }

                    });
                                        
                } else {}
            }, function(response) {
                // failure call back
            });
    }
            if ($scope.TaskView) {
                if ($scope.TaskView.id) {
                    $http.get($scope.apipath + $scope.TaskView.id).then(function(response) {
                        var res = angular.fromJson(response);
                        if (res.data.status == 'ok') {
                            $scope.ToggleTrackingView(res.data.data, 'update');
                        } else {}
                    }, function(response) {
                        // failure call back
                    });
                }
            }
        }
        $scope.parseJsonProfile = function(data) {
            if (data) {
                data = angular.fromJson(data);
                if (data.length) {
                    return data[0];
                }
            }
            return $scope.driverDefaultImg;
        }
        $scope.updateData();
        $scope.task_info = function(data) {
            var info = '<p>ID :#' + data.id + '</p> ' +  '<p>Name :' + data.cust_name + '</p> ' + '<p>Items :' + data.notes + ' ' + '</p> ' + '<p>Status :' + ' ' + data.status + '</p> ';
            if (data.timestamps) {
                info = info + '<p>  Delivered - Date / Time  : ' + data.timestamps + ' </p>';
            }
            if (data.allocated_emp) {
                info = info + '<p>  Delivery Agent : ' + data.allocated_emp + ' </p>';
            }
            return info;
        }
        $scope.RouteDraw = function(data) {
            if (data.allocated_emp) {
                var index = _.findIndex($scope.onlineemp, function(item) {
                    return item.user_id == data.allocated_emp
                })
                if (index > -1) {
                    $scope.RouteDestination = [data.loc_lat, data.loc_lng];
                    $scope.RouteOrigin = [lat, lng];
                    $scope.$apply();
                }
            }
        }
        $scope.emp_info = function($data) {
            var info = '<p>Emp id :' + $data['user']['user_id'] + '</p> ' + '<p>Name :' + $data['user']['last_name'] + ' ' + $data['user']['first_name'] + '</p> ' + '<p> Active : ' + api.timeago($data['timestamp']) + ' </p>';
            return info;
        }
        $scope.PickupIcon = function(){
            return '/assets/images/pick.png';
        }
        $scope.GetIcons = function(status) {
            if (status == "Unallocated") {
                return '/assets/images/Unallocated.png';
            } else if (status == "Allocated") {
                return '/assets/images/Allocated.png';
            } else if (status == "Started Ride") {

                return '/assets/images/On-Progress.png';
            } else if (status == "Incomplete") {
                return '/assets/images/Delivered.png';
            }else if (status == "In Supplier Place") {
                return '/assets/images/On-Progress.png';
            }else if (status == "Products Picked up") {
                return '/assets/images/On-Progress.png';
            }

             else if (status == "Completed") {
                 return '/assets/images/Delivered.png';
             }
            else if (status == "Delivered") {
                return '/assets/images/Delivered.png';
            }           
             else if (status == "Delivered back") {
                return '/assets/images/Canceled.png';
            }
             else {
                return '/assets/images/Home.png';
            }
        }
        $scope.GetEmpIcons = function(emp) {
            // return 'https://openclipart.org/image/2400px/svg_to_png/203366/top-down-bike-racing-monsterbraingames.png'
            var name = 'Offline';



            if (emp) {
                if (emp.user_id) {
                    var status = $scope.getEmpStatusCheck(emp.user_id);
                    if (status == "Free") {
                        var name = 'Free';
                    } else if (status.indexOf("Busy") >= 0) {
                        var name = 'Busy';
                    } else {
                        var name = 'Offline';
                    }
                    if (emp.compass_direction) {
                        if (emp.compass_direction <= 180) {
                            return 'assets/images/' + name + '_left.png';
                        } else {
                            return 'assets/images/' + name + '_right.png';
                        }
                    } else {
                        return 'assets/images/' + name + '_right.png';
                    }
                }
            }
            return 'assets/images/' + name + '_right.png';
        }
        $scope.taskStatus = function() {
            if ($scope.taskFilter == '') {
                $scope.taskFilterSt = false;
            }
        }
        $scope.mapcenter = api.mapcenter;
        $scope.schedule_status = angular.copy(api.schedule);
        $scope.address = '';
        $scope.schedule_status.unshift('');
        $scope.zoom = 18;
        $scope.mapshow = true;
        $scope.pan = function(task, type) {
            // debugger;
            var type = type || 0;
            if (task && type == 'task') {
                $scope.mapcenter = [task.loc_lat, task.loc_lng];
                $scope.zoom = 18;
                $scope.RouteDraw(task);
            }
        }
        $scope.DriverFilterStatus = function() {
            if ($scope.DriverFilterV == 'All') {
                $scope.DriverFilterSt = false;
                $scope.DriverFilter = '';
            }
            if ($scope.DriverFilterV == 'Busy') {
                $scope.DriverFilterSt = true;
                $scope.DriverFilter = 'Busy';
            }
            if ($scope.DriverFilterV == 'Free') {
                $scope.DriverFilterSt = true;
                $scope.DriverFilter = 'Free';
            }
        }
        $scope.DriverGPSFilterStatus = function() {
            // if($scope.DriverGPSFilterV == 'All')
            // {
            //      $scope.DriverGPSFilter='';
            // }
            // if($scope.DriverGPSFilterV == 'Online')
            // {
            //      $scope.DriverGPSFilter='Online';
            // }
            // if($scope.DriverGPSFilterV == 'Offline')
            // {
            //      $scope.DriverGPSFilter='Offline';
            // }
            // if($scope.DriverGPSFilterV == 'Inactive')
            // {
            //      $scope.DriverGPSFilter='Inactive';
            // }
        }


        $scope.GetSpeedStatus = function(user_id)
        {
             if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                    if (item.user_id == user_id) {
                        return true;
                    } else {
                        return false;
                    }
                })

                if (data) {

                    if(data.speed)
                    {
                        if(Math)
                        {
                           data.speed = Math.round( data.speed);
                        }
                        else
                        {

                        }
                        return data.speed + ' kph';
                    }
                    else
                    {
                        return  'No Data';
                    }
                }
                else
                {
                     return  'No Data';
                }

            }
            else
            {
               return  'No Data';
            }
        }

        $scope.GetBatteryStatus = function(user_id)
        {
             if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                                  if (item.user_id == user_id) {
                        return true;
                    } else {
                        return false;
                    }
                })

                if (data) {

                    if(data.battery_status)
                    {
                         return data.battery_status + '%';
                    }
                    else
                    {
                        return  'No Data';
                    }
                }
                else
                {
                     return  'No Data';
                }

            }
            else
            {
               return  'No Data';
            }
        }

        $scope.MobileLast = function(data)
        {
            if(data == 'Active')
            {

              return '#256725';
            }
            else if(data == 'In Active')
            {
                   return '#2e43ff';
            }
            else
            {
                      return '#693f3f';
            }


        }
        $scope.getLastActivity = function(e, color, timestamp) {

            var color = color || false;
            var timestamp = timestamp || false;
            if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                                   // console.log($scope.onlineemp);
                    if (item.user_id == e) {
                        return true;
                    } else {
                        return false;
                    }
                })
                if (data) {
                    if (timestamp) {
                        return api.onlineTimeAgo(data.timestamp);
                    }
                    var check = new Date(data.timestamp);
                    var now = moment(new Date());
                    if (moment(check).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD")) {
                        var diffmm = now.diff(check, 'minutes');
                        // .format("mm");
                        if (parseInt(diffmm) < 10 && parseInt(diffmm) > 0) {
                            if (color) {
                                return '#256725';
                            } else {
                                return "Online";
                            }
                        } else {
                            if (color) {
                                return '#2e43ff';
                            } else {
                                return "Inactive";
                            }
                        }
                    } else {
                        if (color) {
                            return '#693f3f';
                        } else {
                              return "No Data";
                        }
                    }
                }
            }
            if (color) {
                return '#693f3f';
            } else {
                return "No Data";
            }
        }
        // $scope.getEmpStatusCheck
        $scope.criteriaMatch = function(criteria) {
            return function(item) {
                var status = $scope.getEmpStatusCheck(item.user_id);
                if ($scope.DriverFilter == 'Busy' && status == 'Busy') {
                    return true;
                } else if ($scope.DriverFilter == 'Free' && status == 'Free') {
                    return true;
                } else if ($scope.DriverFilter == '') {
                    return true;
                } else {
                    return false;
                }
            };
        };
        $scope.criteriaMatchGPS = function(criteria) {
            return function(item) {
                // var status = $scope.getLastActivity(item.user_id);

                if($scope.DriverGPSFilterV == item.activity)
                {
                    return true;
                }
               else if ($scope.DriverGPSFilterV == 'All') {
                    return true;
                } else {
                    return false;
                }

                // if ($scope.DriverGPSFilterV == 'Active' && status == 'Active') {
                //     return true;
                // } else if ($scope.DriverGPSFilterV == 'In Active' && status == 'Inactive') {
                //     return true;
                // } else if ($scope.DriverGPSFilterV == 'Offline' && status == 'No Data') {
                //     return true;
                // } else if ($scope.DriverGPSFilterV == 'All') {
                //     return true;
                // } else {
                //     return false;
                // }
            };
        };
        $scope.TaskStatusColor = function(status) {
            if (status == "Unallocated") {
                return "#66bb6a";
            } else if (status == "Allocated") {
                return "#043507";
            } else if (status == "In-Progress") {
                return "Green";
            } else if (status == "Incomplete") {
                return "#924324";
            } else if (status == "Delivered") {
                return "#8c8181";
            } else if (status == "Canceled") {
                return "#c90505";
            } else {
                return "#4f4f9c";
            }
        }
        $scope.getEmpStatusCheck = function(e, JobOnly) {
            var JobOnly = JobOnly || false;
            if ($scope.tasksData.data) {
                var tasks = 0;
                var data = _.find($scope.tasksData.data, function(item) {
                    if ((item.allocated_emp_id == e) && (item.status == 'In Supplier Place' || item.status == 'Allocated' || item.status == 'Started Ride' || item.status == 'Products Picked up' )) {
                        return true;
                    } else {
                        return false;
                    }
                })
                if (data) {
                    if (JobOnly) {

                     angular.forEach($scope.tasksData.data, function(item,index) {
                            if ((item.allocated_emp_id == e) && (item.status == 'In Supplier Place' || item.status == 'Allocated'  || item.status == 'Started Ride' || item.status == 'Products Picked up' )) {
                             tasks++;
                            }
                        });


                        return '(' + tasks + ' Task)';
                    }
                    return 'Busy';
                }
            }
            if (JobOnly) {
                return '';
            }
            return 'Free';
        }
        // $scope.driverBusy = false;
        // $scope.driverFree = false;
        // $scope.driverAll = true;
        // $scope.filterEmpStatus = function(val) {
        //     if (val == 1) {
        //         $scope.driverFree = false;
        //         $scope.driverBusy = false;
        //     } else if (val == 2) {
        //         $scope.driverAll = false;
        //         $scope.driverBusy = false;
        //     } else if (val == 3) {
        //         $scope.driverFree = false;
        //         $scope.driverAll = false;
        //     } else {}
        // };
        if (api.manager()) {
            $scope.user_id = '';
            $scope.profile = api.profile();
            $scope.profile().then().then(function(data) {
                $scope.user_id = data.data.user_id;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "address": data.data.street
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        var location = results[0].geometry.location;
                        $scope.mapcenter = location;
                        $scope.zoom = 20;
                        $scope.$apply();
                        // $scope.getpos(location);
                    } else {}
                });
                window.Echo.private(globalapp.channel + 'user-' + $scope.user_id)
                .listen('LocationUpdate', function(e) {
                    var index = _.findIndex($scope.onlineemp, function(item) {
                        return item.user_id == e.data.user_id
                    })

                    var index1 = _.findIndex($scope.users, function(item) {
                        return item.user_id == e.data.user.user_id
                    })


                    if (index1 > -1) {

                              $scope.users[index1] = e.data.user;

                    }
                    if (index > -1) {
                        $scope.onlineemp[index] = e.data;


                    } else {
                        $scope.onlineemp.push(e.data);
                    }
                    $scope.$apply();
                })
                     .listen('TaskUpdateEvent', function(e) {
                        // debugger;
                        if(e.data)
                        {

                    var index1 = _.findIndex($scope.tasksData.data , function(item) {
                        return item.id == e.data.id;
                    })

                    // $scope.TaskViewList=[];
                    //      angular.forEach($scope.tasksData.data, function(item,index) {
                    //        if ((item.id ==  e.data.id)) {

                    //         $scope.TaskViewList[0]=item;
                    //        }
                    //    });

                    // console.log($scope.TaskViewList);
                    // var index2 = _.findIndex($scope.TaskViewList , function(item) {
                    //     return item.id == e.data.id;
                    // })


                    if (index1 > -1) {
                        $scope.tasksData.data[index1] = e.data;
                        // $scope.TaskViewList[index2]=e.data;
                        console.log("ds");
                        if($scope.TaskView)
                        {

                            if($scope.TaskView.id)
                            {
                                if($scope.TaskView.id==e.data.id)
                                {
                             console.log($scope.TaskView);
                              $scope.TaskView = e.data;
                              console.log($scope.TaskView);
                               
                               }
                            }
                        }
                        $scope.$apply();    
                        
                    }
                        }

                    if(($scope.ToggleTrackingViewSingleEmp==false) )
                    {
                        $scope.ToggleTrackingViewSingleEmp=false;
                        $scope.ToggleTrackingViewShowAllEmp = false;
                    }
                    if(($scope.ToggleTrackingViewSingleEmp==true) )
                    {
                        $scope.ToggleTrackingViewSingleEmp=false;
                        $scope.ToggleTrackingViewShowAllEmp = false;
                    }

                     })
                ;
            });
        } else {
            if (api.superAdmin()) {
                var domain = api.getSubDomain() + '-';
            } else {
                var domain = globalapp.channel;
            }
            window.Echo.private(domain + 'current_location').listen('LocationUpdate', function(e) {
                var index = _.findIndex($scope.onlineemp, function(item) {
                    return item.user_id == e.data.user_id
                })


                console.log(index);
                if (index > -1) {
                    $scope.onlineemp[index] = e.data;
                } else {
                    $scope.onlineemp.push(e.data);
                }
                $scope.$apply();
            });
        }

        function GetOnlineEmp() {
            if ($state.current.name == 'admin.company_dashboard') {
                $http.get('location_online_emp').success(function(response) {
                    $scope.onlineemp = [];
                    angular.forEach(response.data, function(element) {
                        $scope.onlineemp.push(element);
                    });
                });
                // // $timeout(GetOnlineEmp, 10000);
            } else {}
        }
        $scope.$on('$viewContentLoaded', function() {
            GetOnlineEmp();
        })
        $scope.geocodeupdateloc = function() {
            if ($scope.address) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "address": $scope.address
                }, function(results, status) {

                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        var location = results[0].geometry.location;
                        $scope.mapcenter = location;
                        $scope.zoom = 20;

                        $timeout(function() { $scope.$apply(); }, 10);
                       // $scope.getpos(location);
                    } else {}
                });
            }
        }
        $scope.ToggleTrackingViewStatus = false;

        $scope.ToggleDefault = function() {
            $scope.ToggleTrackingViewZoom = 20;
            $scope.ToggleTrackingViewCenter = api.mapcenter;
            $scope.ToggleTrackingType = '';
            $scope.ToggleTrackingViewCust = {};
            $scope.ToggleRouteDestination = 'London';
            $scope.ToggleRouteOrigin = 'London';
            $scope.ToggleRouteStatus = false;
            $scope.ToggleTrackingViewSingleEmp = false;
            $scope.ToggleTrackingShowRedraw = false;
            $scope.ToggleGPSData = false;
            $scope.TaskViewEmp_id = '';
            $scope.taskGeoData = [];
            $scope.TaskView = [];
          $scope.TripGeoData = [];
            $scope.TaskViewList = [];
            $scope.TaskSelect = '';

             $scope.TripDuration = '';
             $scope.TripKMS  = '';

              $scope.taskStart = '';
              $scope.taskEnd = '';

                                       $scope.TripGeoSegmentsData  = [];
                                       $scope.TripGeoSegments  = [];
                                       $scope.TripGeoDataStart  = [];
                                       $scope.TripGeoDataStop  = [];

$scope.TripStared = [];
            $scope.ToggleTrackingViewShowAllEmp = false;
            console.log($scope.ToggleTrackingViewCenter);
            $scope.ToggleTrackingViewCust.lat = $scope.ToggleTrackingViewCenter[0];
            $scope.ToggleTrackingViewCust.lng = $scope.ToggleTrackingViewCenter[1];
            if (angular.element("#panelId")[0]) {
                angular.element("#panelId")[0].innerHTML = '';
            }
            $scope.ToggleTrackingViewCust = [];
            $scope.ToggleTrackingViewCustSt = false;
        }
        $scope.ToggleDefault();
        $scope.allocatedTask = function(emp) {

            if(!emp)
            {
                return;
            }
            var status = 'Allocated';
            var emp = emp;
            var taskId = $scope.TaskView.id;
            var data = {
                "status": status,
                "emp": emp
            };
            $http.post('allocateTask/' + taskId, {
                data: data
            }).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    $scope.TaskView = res.data.data;
                    $mdDialog.hide('reload');
                    $mdDialog.show($mdDialog.alert().title('Task Allocated').ok('OK'));
                    $scope.updateData();
                } else {
                    $scope.alert = res.data.data;
                }
            }, function(response) {});
        }

        $scope.ToggleTrackingRedraw = function()
        {

            $scope.ToggleRouteStatus = false;

                             if (angular.element("#panelId")[0]) {
                    angular.element("#panelId")[0].innerHTML = '';
                }
            $timeout(
                function()
                {


                    $scope.$apply();
                      $scope.ToggleRouteStatus = true;
                },
                200
            );


            //      $scope.ToggleRouteStatus = false;
            //                  if (angular.element("#panelId")[0]) {
            //         angular.element("#panelId")[0].innerHTML = '';
            //     }


            //              var index = _.findIndex($scope.onlineemp, function(item) {
            //                         return item.user_id == $scope.TaskViewEmp_id;
            //                     });
            //                      if (index > -1) {

            //                         $scope.ToggleRouteOrigin = [$scope.onlineemp[index].lat, $scope.onlineemp[index].lng];
            //                         $scope.ToggleRouteStatus = true;
            //                     }



            //
             }
        $scope.ToggleTrackingViewDrawDirection = function(data) {

            if ($scope.TaskView.status == 'Unallocated' && $scope.ToggleRouteStatus == false) {

                if(!data.user_id)
                {
                       if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                                  if (item.user_id == data) {
                        return true;
                    } else {
                        return false;
                    }
                })
                    if(!data)
                    {
                        return;
                    }
                }
              }

                           if (angular.element("#panelId")[0]) {
                  angular.element("#panelId")[0].innerHTML = '';
              }
                $scope.ToggleRouteOrigin = [data.lat, data.lng];
                $scope.TaskViewEmp_id = data.user_id;
                $scope.ToggleRouteDestination = [$scope.TaskView.loc_lat, $scope.TaskView.loc_lng];
                $scope.ToggleRouteStatus = true;
            } else {

                             if (angular.element("#panelId")[0]) {
                    angular.element("#panelId")[0].innerHTML = '';
                }
                $scope.ToggleRouteStatus = false;
            }
        }

        $scope.ToggleTrackingView = function(data, type) {
            var data = data || '';
            var type = type || '';
            $scope.ToggleDefault();

            // $scope.updateData();
            $scope.ToggleTrackingType = type;
            $scope.ToggleTrackingViewCenter = $scope.mapcenter;
            if (type == 'update') {
                $scope.ToggleTrackingViewStatus = false;
                $scope.ToggleTrackingType = 'task';
            }


            if($scope.ToggleTrackingType == 'emp')
            {

                            $scope.ToggleTrackingViewCust.lat = $scope.mapcenter[0];
                            $scope.ToggleTrackingViewCust.lng = $scope.mapcenter[1];



            }



            if($scope.ToggleTrackingType == 'task')
            {
                // debugger;
                $scope.TaskView = data;
                $scope.viewrecords = [];
                $scope.pickupdatas = [];
                var addressfields = data.pick_address.split('||,');
                var notesfields = data.notes.split(',');
                var laddfields = data.pickup_ladd.split(',');
                var longfields = data.pickup_long.split(',');
                var i= 0;
                for(;i<addressfields.length;i++){
                    var newdata = {
                        pic_loc: addressfields[i],
                        pic_item: notesfields[i],
                        pic_ladd : laddfields[i],
                        pic_long : longfields[i]
                    };     
                 var newarr = [laddfields[i],longfields[i]];
                 $scope.viewrecords.push(newdata);
                 $scope.pickupdatas.push(newarr);
                 console.log($scope.pickupdatas);
                }
            }
            else if($scope.ToggleTrackingType == 'emp')
            {
                $scope.TaskViewEmp_id = data.user_id;
                $scope.TaskSelect = 'All';



                angular.forEach($scope.tasksData.data, function(item,index) {
                       if ((item.allocated_emp_id ==  $scope.TaskViewEmp_id)) {

                $scope.TaskViewList.push(item);
                       }
                   });


                            var tepDate = moment($scope.date).format('YYYY-MM-DD');


                    $http.get('getMyLocations?date='+tepDate+'&emp='+$scope.TaskViewEmp_id).then(function(data)
                    {
           $scope.TripKMS = data.data.data.distance;
                                       $scope.TripDuration  = data.data.data.time_taken;
                                       $scope.TripGeoData  = data.data.data.geoData;


                                       $scope.TripGeoSegmentsData  = [];
                                       $scope.TripGeoSegments = [];
                                       $scope.TripGeoDataStart  = [];
                                       $scope.TripGeoDataStop  = [];

                                    if($scope.TripGeoData.length > 0)
                                       {



                                        for (var i = 0 ; i <= $scope.TripGeoData.length - 1; i++) {


                                            if($scope.TripGeoData[i]['activity'] == 'Start')
                                            {
                                                $scope.TripGeoDataStart.push([$scope.TripGeoData[i]['lat'],$scope.TripGeoData[i]['lng']]);
                                            }

                                            if($scope.TripGeoData[i]['activity'] == 'Stop')
                                            {
                                                 $scope.TripGeoDataStop.push([$scope.TripGeoData[i]['lat'],$scope.TripGeoData[i]['lng']]);

                                            }

                                            if($scope.TripGeoData[i]['activity'] == 'Start')
                                            {
                                                       $scope.TripGeoSegments.push($scope.TripGeoSegmentsData);

                                                       $scope.TripGeoSegmentsData = [];
                                                     $scope.TripGeoSegmentsData.push([$scope.TripGeoData[i]['lat'],$scope.TripGeoData[i]['lng']]);



                                            }
                                            else
                                            {
                                             $scope.TripGeoSegmentsData.push([$scope.TripGeoData[i]['lat'],$scope.TripGeoData[i]['lng']]);

                                            }


                                        }

                                        }


                                        if($scope.TripGeoSegmentsData.length > -1)
                                        {


                                                     $scope.TripGeoSegments.push($scope.TripGeoSegmentsData);

                                                       $scope.TripGeoSegmentsData = [];
                                        }


                                       if($scope.TripGeoData[0])
                                       {
                                        var index = _.findIndex($scope.onlineemp, function(item) {
                                            return item.user_id == $scope.TaskViewEmp_id;
                                        });
                                        var lat = $scope.onlineemp[index].lat;
                                        var lng = $scope.onlineemp[index].lng;
                                        $scope.mapcenter = [lat,lng];                                                                 
                                            $scope.TripStared = [$scope.TripGeoData[0].lat,$scope.TripGeoData[0].lng];


                                       }


                                        $timeout(function() {$scope.$apply();}, 100);


                    })




            }
            else
            {

            }

            if ($scope.ToggleTrackingViewStatus) {
                $scope.SideBar = true;
                $scope.trackingMap = true;
                $scope.ToggleTrackingViewStatus = false;
            } else {
                $scope.SideBar = false;
                $scope.trackingMap = false;
                $scope.ToggleTrackingViewStatus = true;
                if ($scope.ToggleTrackingType == 'task') {

                     $scope.loadTaskData(data)

                }
            }
             
        

        }

        $scope.empLoadTask = function(id)
        {

            var data = {};

                angular.forEach($scope.TaskViewList, function(item,index) {
                       if ((item.id ==  id)) {


                        data = item;


                       }
                   });


            $scope.TaskView = data;

            if(id=='All')
            {
                data={'all':'All'}
            }

           $scope.loadTaskData(data);
        }


        $scope.loadTaskData = function(data)
        {
            console.log(data);
            if(data.all='All')
            {

                $scope.ToggleTrackingViewSingleEmp = false;
            }
            if (data.allocated_emp) {
                   if ((data.allocated_emp_id) && (data.status == 'In Supplier Place' || data.status == 'Started Ride' || data.status == 'Products Picked up' || data.status == 'Allocated')) {
                    // star Tracking view
                    $scope.ToggleTrackingViewCenter = [data.loc_lat, data.loc_lng];
                    $scope.ToggleTrackingViewCust.lat = data.loc_lat;
                    $scope.ToggleTrackingViewCust.lng = data.loc_lng;
                    $scope.ToggleTrackingViewShowAllEmp = false;
                    $scope.ToggleTrackingViewSingleEmp = true;
                    // $scope.ToggleTrackingViewSingleEmp = false;
                    console.log("hi");
                    console.log($scope.ToggleTrackingViewSingleEmp);

                    $scope.ToggleTrackingViewCustSt = true;
                    $scope.TaskViewEmp_id = data.allocated_emp_id;
                    $scope.ToggleTrackingShowRedraw = true;
                    var index = _.findIndex($scope.onlineemp, function(item) {
                        return item.user_id == $scope.TaskViewEmp_id;
                    });
                     if (index > -1) {

                        $scope.ToggleRouteOrigin = [$scope.onlineemp[index].lat, $scope.onlineemp[index].lng];
                    }

                    $scope.ToggleRouteDestination = [data.loc_lat, data.loc_lng];
                    $scope.ToggleRouteStatus = true;
                }


                if ((data.allocated_emp_id) && (data.status == 'Delivered' || data.status == 'Completed' || data.status == 'Delivered back' || data.status == 'Incomplete')) {
                            // star Tracking view
                            $scope.ToggleTrackingViewCenter = [data.loc_lat, data.loc_lng];
                            $scope.ToggleTrackingViewCust.lat = data.loc_lat;
                            $scope.ToggleTrackingViewCust.lng = data.loc_lng;
                            $scope.ToggleTrackingViewShowAllEmp = false;
                            $scope.ToggleTrackingViewSingleEmp = false;
                            $scope.ToggleTrackingViewCustSt = true;
                            $scope.TaskViewEmp_id = data.allocated_emp_id;



                           // ToggleGPSData



                            $http.get('getTaskSummary/'+$scope.TaskView.id).then(function(response) {
                             // $http.get('assets/app/travel-map/data.json').then(function(response) {


                            var res = angular.fromJson(response);
                            if (res.data.status == 'ok') {
                              $scope.taskGeoData = [];

                              angular.element("#panelId")[0].innerHTML = res.data.data.distance+', Taken '+res.data.data.time_taken
                                $scope.taskStart = moment(res.data.data.start).format("Do MMM, h:mm A");
                                $scope.taskEnd =  moment(res.data.data.end).format("Do MMM, h:mm A");


                                angular.forEach(res.data.data.gpsData, function(value, key) {


                                $scope.taskGeoData.push([value.lat,value.lng]);


                                });
                                           //$scope.TripGeoData  = res.data.data.gpsData;
                                           

                               $scope.TripGeoSegmentsData  = [];
                               $scope.TripGeoSegments = [];
                               $scope.TripGeoDataStart  = [];
                               $scope.TripGeoDataStop  = [];
                                $scope.TripGeoData  = res.data.data.gpsData;


                                if($scope.TripGeoData.length > 0)
                                {
                                    for (var i = 0 ; i <= $scope.TripGeoData.length - 1; i++) {
                                        if($scope.TripGeoData[i]['activity'] == 'Start')
                                        {
                                            $scope.TripGeoDataStart.push([$scope.TripGeoData[i]['lat'],$scope.TripGeoData[i]['lng']]);
                                        }
                                        if($scope.TripGeoData[i]['activity'] == 'Stop')
                                        {
                                             $scope.TripGeoDataStop.push([$scope.TripGeoData[i]['lat'],$scope.TripGeoData[i]['lng']]);

                                        }
                                        if($scope.TripGeoData[i]['activity'] == 'Start')
                                        {
                                                   $scope.TripGeoSegments.push($scope.TripGeoSegmentsData);

                                                   $scope.TripGeoSegmentsData = [];
                                                 $scope.TripGeoSegmentsData.push([$scope.TripGeoData[i]['lat'],$scope.TripGeoData[i]['lng']]);
                                        }
                                        else
                                        {
                                         $scope.TripGeoSegmentsData.push([$scope.TripGeoData[i]['lat'],$scope.TripGeoData[i]['lng']]);

                                        }


                                    }




                                    if($scope.TripGeoSegmentsData.length > -1)
                                    {


                                                 $scope.TripGeoSegments.push($scope.TripGeoSegmentsData);

                                                   $scope.TripGeoSegmentsData = [];
                                    }

                                }
                                 if($scope.TripGeoData[0])
                               {

                                    $scope.mapcenter = [$scope.TripGeoData[0].lat,$scope.TripGeoData[0].lng];
                                   // $scope.TripStared = [$scope.TripGeoData[0].lat,$scope.TripGeoData[0].lng];


                               }


                                       // console.log($scope.TripGeoSegmentsData );
                                       // console.log($scope.TripGeoData );
                                       // console.log($scope.TripGeoSegments);
                               console.log($scope.TripGeoDataStart);
                               console.log($scope.TripGeoDataStop);


                                $timeout(function() {$scope.$apply();}, 100);



    

//                    $scope.taskGeoData = res.data.data.gpsData;



                    $scope.ToggleGPSData = true;
                } else {
                }



                            });
                            // $scope.ToggleTrackingShowRedraw = true;
                            // var index = _.findIndex($scope.onlineemp, function(item) {
                            //     return item.user_id == $scope.TaskViewEmp_id;
                            // });
                            //  if (index > -1) {

                            //     $scope.ToggleRouteOrigin = [$scope.onlineemp[index].lat, $scope.onlineemp[index].lng];
                            // }

                            // $scope.ToggleRouteDestination = [data.loc_lat, data.loc_lng];
                            // $scope.ToggleRouteStatus = true;
                        }
                    }
                    if (data.status == 'Unallocated') {
                        // set allocation mode
                        $scope.ToggleTrackingViewCenter = [data.loc_lat, data.loc_lng];
                        $scope.ToggleTrackingViewCust.lat = data.loc_lat;
                        $scope.ToggleTrackingViewCust.lng = data.loc_lng;
                        $scope.ToggleTrackingViewCustSt = true;
                        $scope.ToggleTrackingViewShowAllEmp = true;
                        $scope.ToggleTrackingShowRedraw = false;
                    }

                      if (data.status == 'Canceled') {
                        // set allocation mode
                        $scope.ToggleTrackingViewCenter = [data.loc_lat, data.loc_lng];
                        $scope.ToggleTrackingViewCust.lat = data.loc_lat;
                        $scope.ToggleTrackingViewCust.lng = data.loc_lng;
                        $scope.ToggleTrackingViewCustSt = true;
                        $scope.ToggleTrackingViewShowAllEmp = false;
                        $scope.ToggleTrackingShowRedraw = false;
                    }

                     setTimeout(function() {  $scope.$apply();      }, 1000);
        }

        setTimeout(function() {
            $scope.RouteDestination = [12.9830, 80.2594];
            $scope.RouteOrigin = [12.90230, 80.2294];
            $scope.$apply();
        }, 1000);
        $scope.setPanel = function(renderer) {


                             if (angular.element("#panelId")[0]) {
                    angular.element("#panelId")[0].innerHTML = '';
                }
            renderer.setPanel(document.getElementById('panelId'));
        }
        $scope.custchangeinphone=function(){
        console.log(dat);
        }
        // $scope.ToggleTrackingView(); //hide everything
        //   $scope.ToggleTrackingView(); //show everything
        //$ scope.ToggleTrackingView(); //hide everything


    }
})();
