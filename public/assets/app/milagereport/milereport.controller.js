(function() {
        'use strict';
        angular.module('app').controller('MileReportCtrl', ['$rootScope', '$state', '$stateParams', '$mdToast', '$timeout', '$scope', '$mdEditDialog', '$q', '$http', '$mdDialog', 'api', MileReportCtrl])
  .directive('exportToCsv',function(){
          return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var el = element[0];
            element.bind('click', function(e){
                var table = document.getElementById("tablexp");
                console.log(table.rows.length);
                //var table = e.target.nextElementSibling;
                var csvString = '';
                for(var i=0; i<table.rows.length;i++){
                    if(i==0){
                    csvString="Order ID,StartTime,EndTime,Distance";
                     csvString = csvString + "\n";
                    }else{
                    var rowData = table.rows[i].cells;
                    for(var j=0; j<rowData.length;j++){
                        csvString = csvString + rowData[j].innerHTML + ",";
                    }
                    csvString = csvString.substring(0,csvString.length - 1);
                    csvString = csvString + "\n";
                }
            }
                csvString = csvString.substring(0, csvString.length - 1);
                    var date = new Date();
                var a = $('<a/>', {
                    style:'display:none',
                    href:'data:application/octet-stream;base64,'+btoa(csvString),
                    download:date.toString()+'.csv'
                }).appendTo('body')
                a[0].click()
                a.remove();
            });
        }
    }
    });
        function MileReportCtrl($rootScope, $state, $stateParams, $mdToast, $timeout, $scope, $mdEditDialog, $q, $http, $mdDialog, api) {

            $scope.addRow = function() {
                $scope.multisearch.push({
                    id: $scope.multisearch.length,
                    column: "",
                    ident: ""
                });
            };
            $scope.deleteRow = function(int) {
                $scope.multisearch.splice(int, 1);
                for (var i = 0; i < $scope.multisearch.length; i++) {
                    $scope.multisearch[i].id = i;
                }
                $scope.updateDataTable();
            };
            $scope.resettable = function() {
                $scope.wwstatus = true;
                    $scope.loaddata();
                $scope.datatable = angular.copy(original);
            };
            $scope.updateDataTable = function() {
                $scope.wwstatus = false;
                var filter = false; //set filter false
                for (var j = 0; j < $scope.multisearch.length; j++) {
                    if ($scope.multisearch[j].ident && $scope.multisearch[j].column) {
                        filter = true; //if a filter exists
                    }
                }
                if (filter) { //if a filter is set
                    if (w) {
                        w.terminate();
                    }
                    w = new Worker("assets/app/core/filter.js");
                    w.postMessage({
                        multisearch: $scope.multisearch,
                        datatable: angular.copy(original), //copy the original,
                        smart: $scope.smart
                    });
                    w.onmessage = function(event) {
                        $scope.datatable = event.data;
                        $scope.wwstatus = true;
                        $scope.$digest();
                    };
                } else {
                    $scope.wwstatus = true;
                    $scope.datatable = angular.copy(original);
                }
            }

            $scope.showform = false;
            $scope.add = true;
            $scope.data = {};
            $scope.activefilter = false;
            $scope.showsearch = false;

            var w;
            var original;
            var old = [];
            $scope.smart = true;
            $scope.wwstatus = true;
            $scope.datatable = {};
            $scope.datatable.data = [];

            $scope.limitOptions = [5, 10, 15, 20, 25, 50, 100];
            $scope.options = {
                limitSelect: true,
                pageSelect: true,
                boundaryLinks: true
            };

            $scope.multisearch = Array();
            $scope.multisearch[0] = {
                id: 0,
                column: "",
                ident: "",
            };





        $scope.datePicker = {
            startDate: new Date(),
            endDate: new Date()
        };



        $scope.query = {
            order: 'schedule_date_time',
            limit: 10,
            page: 1
        };
        $scope.autocolumn = [{
            name: "order_id",
            display: "Order ID"
         },{
            name: "allocated_emp",
            display: "Start Date"
        }, {
            name: "cust_name",
            display: "End Date"
         },{
            name: "timestamps",
            display: "Distance Covered"
        }];

      $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
 $scope.filterData = function() {


            if ($scope.data_form.$valid) {
        if($scope.order_id){

            $http.post('milagereport/' + $scope.emp_id, {
                data: {

                    start_date: moment($scope.datePicker.startDate).format('YYYY-MM-DD'),
                    end_date: moment($scope.datePicker.endDate).format('YYYY-MM-DD'),
                    order_id: $scope.order_id
                }
            }).success(function(response) {

                   var temp = [];

                    angular.forEach(response.data, function(value, key){

                        temp.push(value)

                    });
                    console.log(temp)


                    $scope.temp = temp;
                    original = {};
                    original.data = temp;
                    $scope.data = temp;
                    // console.log(response);
                    // // $scope.temp = response.data;
                    // console.log(temp.distance);

            });

        }else{
                console.log(moment($scope.datePicker.startDate).format('YYYY-MM-DD'));
            $http.post('empgpsdata/' + $scope.emp_id, {
                data: {

                    start_date: moment($scope.datePicker.startDate).format('YYYY-MM-DD H:mm:ss'),
                    end_date: moment($scope.datePicker.endDate).format('YYYY-MM-DD H:mm:ss'),
                    emp_id: $scope.emp_id
                }
            }).success(function(response) {

                   var temp = [];

                    angular.forEach(response.data, function(value, key){

                        temp.push(value)

                    });
                    console.log(temp)


                    $scope.temp = temp;
                    original = {};
                    original.data = temp;
                    $scope.data = temp;


                    // console.log(response);
                    // $scope.temp = response.data;
                    // console.log(temp);
            });
        }

        }else if($scope.order_id){

            $http.post('milagereport/' + $scope.emp_id, {
                data: {

                    start_date: moment($scope.datePicker.startDate).format('YYYY-MM-DD h:mm:ss'),
                    end_date: moment($scope.datePicker.endDate).format('YYYY-MM-DD h:mm:ss'),
                    order_id: $scope.order_id
                }
            }).success(function(response) {

                     var temp = [];

                    angular.forEach(response.data, function(value, key){

                        temp.push(value)

                    });
console.log(temp)


                    $scope.temp = temp;
                    original = {};
                    original.data = temp;
                             $scope.data = temp;

                    // console.log(response);
                    // $scope.temp = response.data;
                    // console.log(temp.distance);

            });

        }
        }
                }
            })();
