(function() {
    'use strict';



    angular.module('app')
        .controller('ProfileCtrl', ['$rootScope','$filter', '$state', '$stateParams', '$mdToast', '$timeout', '$scope', '$mdEditDialog', '$q', '$http', '$mdDialog', 'api', ProfileCtrl])

    function ProfileCtrl($rootScope,$filter,$state, $stateParams, $mdToast, $timeout, $scope, $mdEditDialog, $q, $http, $mdDialog, api) {

        $scope.send_mail = function()
        {
            $http.get('send_mail/').then(function (response){
                $scope.success_data=angular.fromJson(response).data;
                if($scope.success_data.status=='ok')
                {
                     $mdDialog.show(
                                $mdDialog.alert()
                                .title('Thanks for contacting us,we will revert back shortly').ok('OK')
                            );

                }
                              
            });
        }
        
        $http.get('user?active=yes').then(function(response){
        var resdata = angular.fromJson(response);
        $scope.packData_info =[]
         angular.forEach(resdata.data.data, function(item,index) {
            // console.log(item.is_delete);
                            if (item.is_delete!="true") {
                            
                            $scope.packData_info.push(item);
                            // console.log($scope.packData_info);
                            }
                        });

        $http.get('checkusers').then(function(response){
        var resdata = angular.fromJson(response);
        $rootScope.packData = resdata.data;
        $scope.packData = $rootScope.packData[0];
        $scope.packData.packageinfo[0].no_of_emp= parseInt($scope.packData.no_of_emp);
        $scope.packData.no_of_emp= parseInt($scope.packData.no_of_emp)-parseInt($scope.packData_info.length);

        if($scope.packData.no_of_emp<=-1)
        {
            $scope.packData.no_of_emp=0;
        }

        });

        // $scope.apply();

        });
        

       


        

        $scope.title = 'Profile';
        $scope.apipath = 'profile/';
        $scope.data = {};
        $scope.role = api.getrole();

        $scope.pwd = {};
        $scope.pwd.old = '';
        $scope.pwd.new = '';
        $scope.pwd.confirm = '';

        var old = [];



        $scope.reset = function() {

            if ($scope.lfApi) {
                $scope.lfApi.removeAll()
            }
            $scope.data_form.$setPristine();
            $scope.data_form.$setUntouched();

            $scope.pwd_reset_form.$setPristine();
            $scope.pwd_reset_form.$setUntouched();

            $scope.data = {};
            $scope.data.profile_image = [];
            $scope.files = [];
            $scope.isFilesNull = true;
            old = [];
            $scope.pwd = {};
            $scope.pwd.old = '';
            $scope.pwd.new = '';
            $scope.pwd.confirm = '';

            $scope.update_info();
        };

   $scope.geocodeupdatelocpickup = function() {
                var address = $scope.data.street;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            $scope.getpickuppos(location);
                        } else {}
                    });
                }
            }
             $scope.getpickuppos = function(data, zoom) {
                var zoom = zoom || 12;
                $scope.pickup_ladd = data.lat();
                $scope.pickup_long = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            };
                $scope.data.street=$rootScope.timeData.street;
            $scope.geocodeupdatelocpickup();

        $scope.ProfileUpdate = function() {


            if ($scope.data_form.$invalid) {

                return;
            }


            // if ($scope.files.length == 1) {
            //     var upload = api.upload($scope.files, 'img');
            //     upload().then().then(function(res) {

            //         var res = angular.fromJson(res);
            //         if (res.status == 'ok') {

            //             old = $scope.data.profile_image;
            //             $scope.data.profile_image = res.data;
            //             $scope.initput();
            //         } else {
            //             $mdDialog.show(
            //                 $mdDialog.alert()
            //                 .title(res.data).ok('OK')
            //             );
            //         }

            //     });

            // } else {
                $scope.initput();
            // }


        }

        $scope.PasswordReset = function() {

            if ($scope.pwd_reset_form.$invalid) {

                return;
            }


            $http.put('change_password_emp', {
                    data: {
                        'old_password': $scope.pwd.old,
                        'new_password': $scope.pwd.new,
                        'confirm_password': $scope.pwd.confirm,
                    }
                })
                .then(
                    function(response) {

                        var res = angular.fromJson(response);


                        if (res.data.status == 'ok') {

                            $mdDialog.show(
                                $mdDialog.alert()
                                .title('Password Changed').ok('OK')
                            );

                            $scope.reset();




                        } else {



                            $mdDialog.show(
                                $mdDialog.alert()
                                .title(res.data.data).ok('OK')
                            );


                        }


                    },
                    function(response) {


                    }
                );




        }

        $scope.FileDelete = function(file, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this  File ?')
                .targetEvent(ev)
                .ok('Yes, Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {

                $scope.data.profile_image.splice(file, 1);

            }, function() {});

        }






        $scope.initput = function() {
            console.log(this.ctrl.searchText);
            if(!$scope.mail){
                $scope.mail = 0;
            }
            if(!$scope.sms){
                $scope.sms = 0;
            }
             if(!$scope.multipick){
                $scope.multipick = 0;
            }else{
                $scope.multipick = 1;
            }
            $http.put('updateprofile', { data: $scope.data,
                mailnote:$scope.mail,
                smsnote:$scope.sms,
                is_multipick:$scope.multipick,
                    timezone:this.ctrl.searchText
               })
                .then(
                    function(response) {

                        var res = angular.fromJson(response)


                        if (res.data.status == 'ok') {

                            $mdDialog.show(
                                $mdDialog.alert()
                                .title($scope.title + '  Info Updated').ok('OK')
                            );

                            $scope.update_info();


                        } else {

                            $mdDialog.show(
                                $mdDialog.alert()
                                .title(res.data.data).ok('OK')
                            );


                            $scope.data.profile_image = old;


                        }
                    }

                );
        }

        $scope.update_info = function() {
            $http.get('gettimezone/').then(function (response){
                $scope.timeZone=angular.fromJson(response).data;
                console.log($scope.timeZone);                
            });
            $http.get($scope.apipath)
                .then(
                    function(response) {

                        var res = angular.fromJson(response);

                        if (res.data.status == 'ok') {


                            $scope.data = res.data.data;

                            $scope.data.profile_image = angular.fromJson($scope.data.profile_image);
                $rootScope.timeData=$scope.data;
                angular.element('#timezone').val($scope.data.timezone);
                if($rootScope.timeData.mailnote == 1){
                    $scope.mail=1;
                    $scope.mailsts=true;
                    console.log($scope.mailsts);

                }
                if($rootScope.timeData.smsnote == 1){
                    $scope.sms=1;
                    $scope.smssts=true;
                    console.log($scope.smssts);

                }
                if($rootScope.timeData.is_multipick == 1){
                    $scope.multipick=1;
                    $scope.multipicksts=true;

                }

                        } else {


                        }
                    },
                    function(response) {

                    }
                );

$scope.packData = $rootScope.packData[0];
// console.log(parseInt($scope.packData.packageinfo[0].no_of_emp)+1);

 // $scope.begdate = new Date($scope.packData.beg_date);
$scope.end_date = new Date($scope.packData.end_date);
$scope.begdate= $filter('date')($scope.packData.beg_date, "MM dd YYYY");
$scope.end_date= $filter('date')($scope.packData.end_date, "MMM DD, YYYY");


console.log($scope.pack_end_date);
$scope.current_date = new Date();


$scope.end_date=moment($scope.end_date);
$scope.end_date1 = $scope.end_date.format("YYYY-MM-DD");
$scope.pack_end_date=$scope.end_date.format("MMM DD, YYYY");
// $scope.current_date= $filter('date')($scope.current_date, "MM dd YYYY");

$scope.current_date=moment().format("YYYY-MM-DD");
$scope.two_days_b4 = $scope.end_date.subtract(2, "days").format("YYYY-MM-DD");
$scope.one_days_b4 = $scope.end_date.add(1, "days").format("YYYY-MM-DD");
// $scope.end_date1 = $scope.end_date.subtract(0, "days").format("DD-MM-YYYY");

console.log($scope.two_days_b4);
console.log($scope.one_days_b4);
var check = moment($scope.current_date).isBetween($scope.two_days_b4, $scope.end_date1);
console.log(check)
console.log($scope.current_date);
console.log($scope.end_date);
console.log($scope.end_date1);
var isSame = moment($scope.end_date).isSame($scope.current_date);

// if($scope.current_date==$scope.two_days_b4 || $scope.current_date==$scope.one_days_b4 || $scope.current_date==$scope.end_date1)
// {
//     $scope.is_expired_twodays = true;
// }
// else
// {
//     $scope.is_expired_twodays = false;
// }
if(check || isSame)
{
     console.log("Between and same date");
    $scope.is_expired = false;
    $scope.is_expired_twodays = true;
}
else
{
    console.log("Before Current Date");
    var isBefore = moment($scope.end_date).isBefore($scope.current_date);
    
    console.log(isBefore);

    if(isBefore)
    {
    $scope.is_expired = true;
    $scope.is_expired_twodays = false;
    }
    

}
$scope.begdate=moment($scope.begdate).format('MMM DD, YYYY');

 $scope.timezone=$rootScope.timeData.timezone;
                if($rootScope.timeData.mailnote == 1){
                    $scope.mail=1;
                    $scope.mailsts=true;

                }
                if($rootScope.timeData.smsnote == 1){
                    $scope.sms=1;
                    $scope.smssts=true;
                }
                 if($rootScope.timeData.is_multipick == 1){
                    $scope.multipick=1;
                    $scope.multipicksts=true;
                }
        }


         $scope.searchTextChange=function(text) {
      $log.info('Text changed to ' + text);
    }

    $scope.selectedItemChange=function(item) {
        console.log(item)
      $log.info('Item changed to ' + JSON.stringify(item));
    }

$scope.querySearch =function(query) {
       var deferred = $q.defer();
           
                var states = $scope.timeZone.filter(function(state) {
                   
                    return (state.desc.toUpperCase().indexOf(query.toUpperCase()) !== -1 || state.desc.toUpperCase().indexOf(query.toUpperCase()) !== -1);
                });

                deferred.resolve(states);
                console.log(deferred.promise);
            return deferred.promise;
    }



        $scope.update_info();



    }
})();
