(function() {
    'use strict';
    angular.module('app').controller('AppSidenavRightCtrl', ['$scope', '$http', 'api', '$timeout', '$rootScope','$mdSidenav', AppSidenavRightCtrl])

    function AppSidenavRightCtrl($scope, $http, api, $timeout, $rootScope,$mdSidenav) {
        $scope.messages_mob = [];
        $scope.messages_web = [];
        $scope.notifications = [];
        $scope.newcount = 0;
        $scope.locations = [];
        $scope.notificationsPage = 1;
        $scope.tempcus = api.customers();
        $scope.tempcus().then().then(function(data) {
            $scope.locations = data.data;
        });
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;

        });
         $scope.toggleRight = buildToggler('right');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    }
        $scope.parseInt = parseInt;
        $scope.Increment = function() {
            $scope.newcount = $scope.newcount + 1;
            $timeout(function() {
                $rootScope.$digest()
            }, 10);
        }

        $scope.Decrement = function() {
            $scope.newcount = $scope.newcount - 1;
            $timeout(function() {
                $rootScope.$digest()
            }, 10);
        }
        $scope.ReadMyNotifications = function(data) {
            // debugger;
            $scope.notifications.length=$scope.notifications.length-1;   
            
            if($scope.notifications.length<=0)
                {
                    $scope.notifications=[];
                }
            var id = _.findIndex($scope.notifications, ['id', data]);
            if (typeof id !== 'undefined') {
                $scope.notifications[id].read_at = new Date();
                $scope.Decrement();
                var id = $scope.notifications[id].id;
                $http.get('ReadMyNotifications/' + id).success(function(response) {});

            }
        }
        $scope.readall = function(){
                var id = '000';
                $http.get('ReadMyNotifications/' + id).success(function(response) {});
                for(var read=0;read<$scope.notifications.length;read++){
                $scope.notifications[read].read_at = new Date();  
                $scope.newcount = $scope.newcount - 1;
                $scope.notifications=[];
            }
                        $scope.GetNotify();
        }
        $scope.GetNotify = function() {
            $http.get('GetMyNotifications?page=' + $scope.notificationsPage).success(function(response) {
                var alert = false;
                $scope.newcount = $scope.newcount + response.total;
                $scope.notificationsPage = response.current_page + 1;
                angular.forEach(response.data, function(element) {
                    $scope.notifications.push(element);
                });
            })
        };
        $scope.GetNotify();
        $rootScope.$on('NotificationEvent', function(e, args) {
            if (args) {
                $scope.Increment();
                globalapp.alert();
                $scope.notifications.push(args.data)
            }
        });

    }
})();
