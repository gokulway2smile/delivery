@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        <div style = "background-color : white">
            <img src="{{ config('app.logo') }}" style="height: 100px;" />
        </div>
    @endslot

    # <b>Hello,</b>

<p>Welcome to ManageTeamz – Delivery Management System.</p>

<p>Thanks for using Manage Teamz. We are adding new features every month based on your feedback and Happy to solve delivery and fleet management problem. We have also brought in a free trial period for 14 days and your package will expire in the next 14 days.  If you are Interested in upgrading to a paid version, please reach us at bd@manageteamz.com.</p>


Thanks!<br>
ManageTeamz Team<br>
Call : + 91 73387 73388<br>
	   +1-630 299 7737<br>
Email : bd@manageteamz.com<br>
Twitter : <a href="https://twitter.com/ManageTeamz">@ManageTeamz</a><br>

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
     
            <table align="center" width="570" cellpadding="0" cellspacing="0">
                <tr>
                    <td">
                        <p">
                            &copy; {{ date('Y') }}
                            <a" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a>.
                            All rights reserved.
                        </p>
                    </td>
                </tr>
            </table>

        @endcomponent
    @endslot
@endcomponent
