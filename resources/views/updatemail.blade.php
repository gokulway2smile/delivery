<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
    .pl-c{color:#969896}.pl-c1,.pl-s .pl-v{color:#0086b3}.pl-e,.pl-en{color:#795da3}.pl-s .pl-s1,.pl-smi{color:#333}.pl-ent{color:#63a35c}.pl-k{color:#a71d5d}.pl-pds,.pl-s,.pl-s .pl-pse .pl-s1,.pl-sr,.pl-sr .pl-cce,.pl-sr .pl-sra,.pl-sr .pl-sre{color:#183691}.pl-v{color:#ed6a43}.pl-id{color:#b52a1d}.pl-ii{background-color:#b52a1d;color:#f8f8f8}.pl-sr .pl-cce{color:#63a35c;font-weight:700}.pl-ml{color:#693a17}.pl-mh,.pl-mh .pl-en,.pl-ms{color:#1d3e81;font-weight:700}.pl-mq{color:teal}.pl-mi{color:#333;font-style:italic}.pl-mb{color:#333;font-weight:700}.pl-md{background-color:#ffecec;color:#bd2c00}.pl-mi1{background-color:#eaffea;color:#55a532}.pl-mdr{color:#795da3;font-weight:700}.pl-mo{color:#1d3e81}
  </style>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>

</head>
<body>

<div class="container">
  <h2>Notifications to All Users</h2>
  <form action="/updatemail" method="POST" onsubmit="return confirm('Do you really want to submit the form?');">


<input name="_token" type="hidden" value="{{ csrf_token() }}">

    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" required="" id="pwd" placeholder="Enter password" name="pwd">
    </div>

    <div class="form-group">
      <label for="pwd">Subject:</label>
      <input type="text" class="form-control"  required="" placeholder="Enter Email Subject" name="subject">
    </div>

    
    <textarea id="demo1" name="body"></textarea>
    

    <button type="submit" class="btn btn-default" required="">Send Email</button>
  </form>
</div>


<script>

  new SimpleMDE({
    element: document.getElementById("demo1"),
    spellChecker: false,
  });
  

function validate(form) {

    if(!valid) {
        alert('Please correct the errors in the form!');
        return false;
    }
    else {
        return confirm('Do you really want to submit the form?');
    }
}

  </script>


</body>
</html>