@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        <div style = "background-color : white">
            <img src="{{ config('app.logo') }}" style="height: 100px;" />
        </div>
    @endslot

    # <b>Hello,</b>

<p>Welcome to ManageTeamz – Delivery Management System.</p>

<p>Your trial period is going to expire.You wil not be able to add driver/task,please contact us to upgrade your account. You can also respond to this email and we will revert back shortly.</p>


Thanks!<br>
ManageTeamz Team<br>
Call : + 91 73387 73388<br>
	   +1-630 299 7737<br>
Email : bd@manageteamz.com<br>
Twitter : <a href="https://twitter.com/ManageTeamz">@ManageTeamz</a><br>

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
     
            <table align="center" width="570" cellpadding="0" cellspacing="0">
                <tr>
                    <td">
                        <p">
                            &copy; {{ date('Y') }}
                            <a" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a>.
                            All rights reserved.
                        </p>
                    </td>
                </tr>
            </table>

        @endcomponent
    @endslot
@endcomponent
