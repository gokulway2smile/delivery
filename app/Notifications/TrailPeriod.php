<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\PushChannel;

class TrailPeriod extends Notification
{
    use Queueable;
    private $user;
    public $push_title;
    public $push_body;
    public $push_payload;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Models\User $user)
    {
       $this->user = $user;
       $this->push_title = 'Trail Period Expiration';
        $this->push_body = 'Update your Package to use our App ';
        $this->push_payload = $user->toArray();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database',PushChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        $data = (new MailMessage)
            ->subject('Package Upgrade')
            // ->bcc('bd@manageteamz.com')
            ->markdown('trail',['user'=>$notifiable]);
        return $data;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => '',
        ];
    }
     public function toPush($notifiable)
    {

    }
}
