<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\Base;
use App\Models\EmpCustSchedule as task;
use App\Models\EmpSchedule as allocation;
use App\Models\ScheduleTaskStatus;
use App\Models\TravelHistory as api;
use App\Models\distance;
use Toin0u\Geotools\Facade\Geotools;
use \DateTime;
use \DateTimeZone;
use App\Models\User;


class TaskDuration implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user_id;
    protected $task_id;
    protected $apicall;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($empid,$task_id)
    {
        $this->task_id = $task_id;
        $this->user_id = $empid;
        $this->apicall = true;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      try
        {       
        $task = task::where('id', $this->task_id)->with('all_status')->first();
        if($this->user_id)
        {
            $task = $task->toArray();
         // print_r($task);
        }

    }
    catch (\Exception $e)
    {

                return Base::touser('Task not found');
    }
//echo "data";

            $taskStatus = array_reverse($task['all_status']);
            $picks = array_first($taskStatus, function ($value, $key) use ($task) {

                if ($this->user_id == $task['allocated_emp_id']) {
                    return $value['status'] == 'Products Picked up';
                }

            });


            $ride = array_first($taskStatus, function ($value, $key) use ($task) {
                if ($this->user_id == $task['allocated_emp_id']) {
                    return $value['status'] == 'Started Ride';
                }

            });
            $allocated = array_first($taskStatus, function ($value, $key) use ($task) {

                if ($this->user_id == $task['allocated_emp_id']) {
                    return $value['status'] == 'In Supplier Place';
                }

            });
            $Delivered = array_first($taskStatus, function ($value, $key) use ($task) {

                if ($this->user_id == $task['allocated_emp_id']) {

                    return $value['status'] == 'Delivered';
                }

            });
            $Incomplete = array_first($taskStatus, function ($value, $key) use ($task) {

                if ($this->user_id == $task['allocated_emp_id']) {
                    return $value['status'] == 'Delivered back';
                }
            });
              
			
            if ($picks || $ride) { 
                                
                    if($ride){
                        $Progress=$ride;
                    }
                    if ($Progress['timestamps']) {
                        $start = $Progress['timestamps'];
                    } else {
                        $start = $Progress['created_at'];
                    }

                    if ($Delivered['timestamps']) {
                        $endtime = $Delivered['timestamps'];
                    } else {
                        $endtime = $Delivered['created_at'];
                    }
                    //$end = isset($end) ? $end : date('Y-m-d H:i:s');

                    // $end = "2017-07-04 05:35:00";
                   // print_r(Base::tomysqldatetime($start));
                   // print_r(Base::tomysqldatetime($endtime));

                    $start = $start;
                                $gpsData = api::
                        where('user_id', $task['allocated_emp_id'])->
                        where('created_at', '<=', Base::tomysqldatetime($endtime))->
                        where('created_at', '>=', Base::tomysqldatetime($start))->
                        get()->toArray();
                        
                    $distInMeter = [];
                     $distInMeter[] = 0;
                    for ($x = 0; $x < count($gpsData) - 1; $x++) {

                        if( ($gpsData[$x]['activity'] == 'Start'))
                        {

                            $distInMeter[] =  $distInMeter[count( $distInMeter)-1];
                            $distInMeter[] = 0;
                        }
                        else
                        {
                        $data1                   = $gpsData[$x];
                        $data2                   = $gpsData[$x + 1];
                        $gpsData[$x]['path']     = [$data1['lat'], $data1['lng']];
                        $gpsData[$x + 1]['path'] = [$data2['lat'], $data2['lng']];
                        $coordA                  = Geotools::coordinate($gpsData[$x]['path']);
                        $coordB                  = Geotools::coordinate($gpsData[$x + 1]['path']);
                        $distance                = Geotools::distance()->setFrom($coordA)->setTo($coordB);
                        $distInMeter[count( $distInMeter)-1]  = $distance->flat() + $distInMeter[count( $distInMeter)-1];
                        }


                    }

                    $distInMeter = array_sum($distInMeter);

                    $time_taken = Base::time_elapsed_string($endtime, true, $start);

                    if (empty($time_taken)) {
                        $time_taken = '1 min';

                    }

                    $distInMeter = $distInMeter / 1000;
                    $data = User::where('user_id', '=', $task['allocated_emp_id'])->first();
        $id = $data->belongs_manager;
        $timedata = User::where('user_id', '=', $id)->get();
       $toTz= $data->timezone=$timedata[0]['timezone'];
                        $date = new DateTime($start, new DateTimeZone('UTC'));
                        $date->setTimezone(new DateTimeZone($toTz));
                        $time= $date->format('Y-m-d H:i:s');
                        $edate = new DateTime($endtime, new DateTimeZone('UTC'));
                        $edate->setTimezone(new DateTimeZone($toTz));
                        $etime= $edate->format('Y-m-d H:i:s');


                        $distance = new distance();
                        $distance->emp_id=$task['allocated_emp_id'];
                        $distance->distance=round($distInMeter, 2) . ' kms';
                        $distance->task_id=$this->task_id;
                        $distance->time_taken=$time_taken;
                        $distance->order_id=$task['order_id'];
                        $distance->start_time=$time;
                        $distance->end_time=$etime;
                        $distance->save();
                        $taskdetails = task::where('id', $this->task_id)->first();
                        $taskdetails->exact_delivery=$etime;
                        $taskdetails->update();
                            // 'time_taken' => $time_taken,
                            // 'start'      => $start,
                            // 'end'        => $endtime,
                            // 'gpsData'    => $gpsData,
                            // 'distance'   => ,
                            // 'emp_id'     => $task['allocated_emp_id'],
                    

                       // print "Data";

            }
    }
}
