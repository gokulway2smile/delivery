<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class Activity
 */
class distance extends Model implements AuditableContract
{
    use Auditable;


    protected $table = 'distance';

    public $timestamps = true;
}
