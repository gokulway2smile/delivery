<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class Activity
 */
class UserPackage extends Model implements AuditableContract
{
    use Auditable;


    protected $table = 'user_package_table';

    public $timestamps = true;
    protected $guarded = [];
     public function packageinfo()
    {
        return $this->hasMany('App\Models\packageinfo','id','package_id');
    }

}
