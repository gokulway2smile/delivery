<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class Activity
 */
class history extends Model implements AuditableContract
{
    use Auditable;


    protected $table = 'carbage_history';

    public $timestamps = true;

    protected $guarded = [];
}
