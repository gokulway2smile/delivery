<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class Activity
 */
class packageinfo extends Model implements AuditableContract
{
    use Auditable;


    protected $table = 'package_info';

    public $timestamps = true;


    protected $guarded = [];
}

