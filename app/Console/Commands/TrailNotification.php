<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\UserPackage;
use Mail;
use \DateTime;

class TrailNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trails:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $date = new DateTime;
                           
        $date= $date->format('Y-m-d');
        $cur_date = date('Y-m-d',  strtotime($date));
        $date = date('Y-m-d', strtotime('-2 days', strtotime($date)));

       // command($date);
      $userPackage = UserPackage::where([['end_date','>=',$date],['end_date','<=',$cur_date]])->get();

        foreach ($userPackage as $use => $user) {
         
          $users_val=User::where([['user_id','=',$user->user_id],['role_id','=','2']])->first()->toArray();
           $email = $users_val['email'];
           // $content = "Your trial period is going to expire. You are not able to add drivers / tasks. Please contact us to upgrade your account";
//            Mail::send('trail', $users_val, function($message) use ($users_val) {

//                 $message->to($users_val['email'], $users_val['first_name'])
//                 ->from('from@from.com', 'fromName')
//                 ->replyTo('from@from.com', 'fromName')
//                 ->subject("Package Upgrade!");

// });
            
            // $user = \App\Models\User::find(46016);//46158
            $user = \App\Models\User::find($user->user_id);
            
            $notification =  $user->notify(new \App\Notifications\TrailPeriod($user));
           event(new \App\Events\NotificationEvent($user));
            // // $user->notify(new \App\Notifications\TrailPeriod($users_val));
            //      event(new \App\Events\NotificationEvent($user));

      }

    }
}
