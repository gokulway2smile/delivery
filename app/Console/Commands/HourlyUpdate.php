<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Models\Customer;
use App\Models\User;
use App\Models\timezone as timezonemang;

use App\Models\EmpCustSchedule as task;
use App\Models\EmpSchedule as allocation;
use App\Models\ScheduleTaskStatus;
use App\Http\Controllers\ApiEmpScheduleController; 
use App\Models\TravelHistory as api;
use App\Http\Controllers\Base;


class HourlyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:system';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Order Systems Integrations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
  $client = new Client();
$store = $client->request('GET','https://www.localisbig.com/api/1/entity/ms.store_locations',[
  'headers'        => ['access-key' => 'b9696cb420a19dd3f59ddbd5bd212d61'],
]);
$storedata =  $store->getBody();
$stdata = json_decode($storedata)->data;


$res = $client->request('GET', 'https://www.localisbig.com/api/1/entity/ms.orders?sort=[{"field":"updated_on",order : 1}]', [
    'headers'        => ['access-key' => 'b9696cb420a19dd3f59ddbd5bd212d61'],

]);
if($res->getStatusCode() == 200){
$data =  $res->getBody(); // { "type": "User", ....
//echo $data;
$order_datas = json_decode($data)->data;
echo  count($order_datas);
foreach ($order_datas as $newdata => $order_data) {
  # code...
$receiver_address = $order_data->shipping_address;
$receiver_phone = $receiver_address->phone;
$receiver_name = $receiver_address->full_name;
$receiver_address = $receiver_address->address.$receiver_address->city.$receiver_address->state;
//print_r($receiver_address);

$pickup_point = $order_data->seller_name;
$items = $order_data->items;
$order_id = $order_data->order_id;
$notes = '';
$cmt = '';
$mob = $order_data->payment_method->type;
if(isset($receiver_email -> $order_data->email)){
  $receiver_email = $order_data->email;
}else{
  $receiver_email = '';
}

foreach ($order_data->items as $item => $dataitem) {
  # code...
  if($notes != ''){
    $notes = $notes.','.$dataitem->name;
  }else{
    $notes = $dataitem->name;
  }
  
}
echo count($order_data->items);
if(isset($order_data->options->select_delivery_time)){
  $len = strlen($order_data->options->select_delivery_time);
  $strtime = substr($order_data->options->select_delivery_time,($len - 8));
  $curtime = strtotime('today'.$strtime);
  $delivery_date = date('Y-m-d H:i:s', $curtime);
}elseif (isset($order_data->options->select_delivery)) {
  $len = strlen($order_data->options->select_delivery);
  $strtime = substr($order_data->options->select_delivery,($len - 8));
  $curtime = strtotime('today'.$strtime);
  $delivery_date = date('Y-m-d H:i:s', $curtime);
}  
$pickup_add = '';
$pickup_ladd = '';
$pickup_long = '';
  $newdata = json_encode($order_data->shipping_by_vendor);
  $ventors = json_decode($newdata,true);
  foreach ($ventors as $vent => $ventor) {
    try{
      if(isset($order_data->available_shipping_charges)){
        $strdata = $order_data->available_shipping_charges->$vent;
      }
    }
    //return $vent;
    catch(Exception $e){
            return $order_data->available_shipping_charges;
      //return $strdata;
    }
foreach ($stdata as $st => $stored) {
        if($stored->user_id == $strdata[0]->seller){
            if($stored->address != ''){
             if(isset($stored->latitude)){
              if($pickup_add != ''){
                $pickup_add = $pickup_add."||,". $stored->address;
              $pickup_ladd = $pickup_ladd.",".$stored->latitude;
              $pickup_long = $pickup_long.",".$stored->longitude;
               
              }else{
               $pickup_add = $stored->address;
              $pickup_ladd = $stored->latitude;
              $pickup_long = $stored->longitude;
                
              }
              // echo $pickup_ladd;
             }
               
            }else{
              echo "string";
            }
            
        }
    } 
  };

  if(isset($order_data->client_details->geo_location->ll[0])){
$lat = $order_data->client_details->geo_location->ll[0];
$long = $order_data->client_details->geo_location->ll[1];
}else{
  $lat = '';
  $long = '';
}
  $orders= task::where('order_id',$order_id)->get();
  if(count($orders) == 0 ){
   try{
             $task                     = new task();
        $task->schedule_date_time = $delivery_date;        
            $Customer             = new Customer();
            $Customer->name       = $receiver_name;
            $Customer->email      = $receiver_email;
            $Customer->emp_id     = 1;
            $Customer->address    = $receiver_address;
            $Customer->loc_lat    = $lat;
            $Customer->loc_lng    = $long;
            $Customer->contact_no = $receiver_phone;
            $Customer->save(); 
        $task->cust_id        = $Customer->id;
        $task->notes          = $notes;
        $task->pick_address   = $pickup_add;
        $task->order_id       = $order_id;
        $task->comments       = $cmt;
        $task->mob            = $mob;
        $task->added_by       = 1;
        $task->cust_email     = $receiver_email;
        $task->sender_name    = '';
        $task->sender_number  = '';
        $task->sender_number  = '';
        $task->picktime       = Base::tomysqldatetime(date("Y-m-d H:i:s"));
        $task->pickup_long    = $pickup_ladd;
        $task->pickup_ladd    = $pickup_long;
        $task->sent_address   = '';
        $task->status         = 'Unallocated';
        $task->method         = 'pickup';
        $task->is_new_address = true;
        
            $task->cust_phone   = $receiver_phone;
            $task->loc_lat      = $lat;
            $task->loc_lng      = $long;
            $task->cust_address = $receiver_address;
    
       
        $task->save();
        
        $task_status             = new ScheduleTaskStatus();
        $task_status->emp_id     = 1;
        $task_status->task_id    = $task->id;
        $task_status->address    = '';
        $task_status->lat        = '';
        $task_status->long       = '';
        $task_status->status     = 'Unallocated';
        $task_status->timestamps = Base::tomysqldatetime(date("Y-m-d H:i:s"));
        $task_status->save();  

   }catch(Exception $e){

   } 
  }
}
}
    
  }
  

    
}
