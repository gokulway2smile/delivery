<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use App\Models\packageinfo;
use App\Models\UserPackage;
use App\Models\ScheduleTaskStatus as task;

use App\Models\Customer as Customers;
use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class UserController extends Controller
{
    public function getalluser(Request $request)
    {
        $data = User::with('role')
                    ->where('is_active', 1)
                     ->whereIn('role_id', [1,2])->
                    get()->toArray();
                    foreach ($data as $key => $value) {

                    $data[$key]['role'] = $data[$key]['role']['display_name'];

                }
                 return Base::touser($data, true);
    }
    public function update_package(Request $request)
    {
        
        $beg_date = '2018-06-01';
        $end_date = '2018-06-24';
        // echo $dt;
        $userPackage = UserPackage::where([['beg_date','<=',$beg_date]])->get();

          foreach ($userPackage as $use => $user) {
         
          $users_val=User::where([['user_id','=',$user->user_id],['role_id','=','2']])->first()->toArray();
             if($users_val['user_id'])
            {   
            $email = $users_val['email'];
            $userPackage = UserPackage::find($user->id);
            $userPackage->end_date=$end_date;
            $userPackage->save();

            // $user = \App\Models\User::find(46158);
            try{
            $user = \App\Models\User::find($user->user_id);

            if($user->user_id)
            {            
                $notification =  $user->notify(new \App\Notifications\Packageupdate($user));
            event(new \App\Events\NotificationEvent($user));
            }
            }
            catch (\Exception $e) {
                echo $user->user_id.'-';
                Mail::raw($userPackage, function ($message){
            $message->to('abinayah@way2smile.com');
            });
            }

      } 
       Mail::raw($userPackage, function ($message){
            $message->to('abinayah@way2smile.com');
 });
        // Mail::raw($userPackage, function($message) use ($userPackage) {

        //         $message->to('abinayah@way2smile.com',"Abi")
        //         ->from('bd@manageteamz.com', 'fromName')
        //         ->replyTo('bd@manageteamz.com', 'fromName')
        //         ->subject("Package Upgrade!");
        //     });


        return Base::touser($userPackage, true);
    }
}

    public function index(Request $request)
    {

        // try {
            if ($this->admin || $this->backend) {

                if ($request->input('active')) {
                    $data = User::with('role')
                    ->where('is_active', 1)
                     ->whereIn('role_id', [1,2])->
                    get()->toArray();

                } else {
                    $data = User::with('role')->get()->toArray();
                }

                foreach ($data as $key => $value) {

                    $data[$key]['role'] = $data[$key]['role']['display_name'];

                }
                return Base::touser($data, true);

            } elseif ($this->manager) {

                if ($request->input('active')) {

                    $data = User::with('role')
                         ->where('is_active', 1)
                        ->whereIn('role_id', [1,2])
                        ->whereNotIn('is_delete',[true,"true"])
                        ->where('belongs_manager', $this->emp_id)
                        ->orWhere('user_id', $this->emp_id)

                        ->get()->toArray();
                } else {

                    $data = User::with('role')->where('belongs_manager', $this->emp_id)->
                        orWhere('user_id', $this->emp_id)
                        ->get()->toArray();
                }

                foreach ($data as $key => $value) {

                    $data[$key]['role'] = $data[$key]['role']['display_name'];

                }

                return Base::touser($data, true);
            } else {
                return Base::throwerror();
            }
        // } catch (\Exception $e) {
        //     return Base::throwerror();
        // }
    }

    public function resetpassword(Request $request)
    {
        $rules = [
            'new_password'     => 'required',
            'confirm_password' => 'required|same:new_password',
            'user_id'          => 'exists:user,user_id',
        ];

        $data = $request->input('data');

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        // try {
            if ($this->admin || $this->backend) {
                $reset = User::find($data['user_id']);
            } elseif ($this->manager) {
                $reset = User::where('user_id',$data['user_id'])->first();
            } else {
                return Base::throwerror();
            }

            $reset->user_pwd = encrypt($data['new_password']);

            $reset->save();

            return Base::touser('Password Changed', true);
        // } catch (\Exception $e) {
        //     return Base::throwerror();
        // }
    }

    public function resetpasswordbyemp(Request $request)
    {
        $rules = [
            'new_password'     => 'required',
            'confirm_password' => 'required|same:new_password',
        ];

        $data = $request->input('data');

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        try {
            $reset           = User::find($this->emp_id);
            $reset->user_pwd = encrypt($data['new_password']);
            $reset->save();
            return Base::touser('Password Changed', true);
        } catch (\Exception $e) {
            return Base::throwerror();
        }
    }

    public static function roles()
    {
        return Base::touser(UserRole::all(), true);
    }

    public static function managers()
    {
        try {
            $manager = UserRole::where('name', Base::manager())->get()->toArray();

            $role = $manager[0]['role_id'];

            return Base::touser(User::where('role_id', $role)->get(), true);
        } catch (\Exception $e) {
            return Base::throwerror();
        }
    }

    public function store(Request $request)
    {
        // 
        $rules = [
            'role_id'    => 'required',
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone'      => 'required',
            // 'street'        => 'required',
            // 'city'          => 'required',
            // 'state'         => 'required',
            //'zipcode'       => 'required',
            // 'country'       => 'required',
            // 'profile_image' => 'required',
            'email'      => 'required|email|unique:user',
        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

         $totalcount = User::where('is_delete','false')->where('belongs_manager',$this->emp_id)->count();
          $emps = UserPackage::where('user_id',$this->emp_id)->first();
          $packageinfo = packageinfo::where('id',$emps['package_id'])->first();
        $packageinfo['no_of_emp'];
       $emps->no_of_emp = $emps['no_of_emp'] - $totalcount;
       if($emps->no_of_emp>0)
       {

        $user             = new User();
        $user->role_id    = $data['role_id'];
        $user->first_name = $data['first_name'];
        $user->last_name  = $data['last_name'];
        $user->user_pwd   = encrypt(strtolower(str_random(5)));
        $user->phone      = $data['phone'];
        $user->email      = $data['email'];
       //$user->is_delete   = true;
        $user->is_delete      = $data['active'];
       // $user->comments    = $data['comments'];
        $user->comments1 = $request->get("comments1");
        $user->comments2  = $request->get("comments2");
        $user->comments3    = $request->get("comments3");
        $user->comments4 = $request->get("comments4");
        $user->comments5  = $request->get("comments5");
        $user->whatsapp = $request->get("whatsapp");
        $user->resaddress  = $request->get("resaddress");
        $user->city    = isset($data['city']) ? $data['city'] : null;
        $user->street  = isset($data['street']) ? $data['street'] : null;
        $user->state   = isset($data['state']) ? $data['state'] : null;
            $user->zipcode = isset($data['zipcode']) ? $data['zipcode'] : 0;
        $user->country = isset($data['country']) ? $data['country'] : null;

        $user->profile_image = isset($data['profile_image']) ? json_encode($data['profile_image'], true) : '[]';

        $user->phone_imei = isset($data['phone_imei']) ? $data['phone_imei'] : '';
        $user->is_active  = isset($data['is_active']) ? $data['is_active'] : 0;

        if ($data['role_id'] == 1) {

            if ($this->admin || $this->backend) {

                if (empty($data['belongs_manager'])) {

                    return Base::touser('Sale Person must have belongs to manager');
                }

                $user->belongs_manager = isset($data['belongs_manager']) ? $data['belongs_manager'] : null;

            } elseif ($this->manager) {

                $user->belongs_manager = $this->emp_id;

            } else {
            }
        }

        $user->save();


            if ($data['role_id'] == 2)
            {


                $user->belongs_manager = $user->user_id;

                  $user->save();

            }
            $restrictcontroller = new restrictcontroller();
         $restrictcontroller->updatecount();
        \App\Http\Controllers\NotificationsController::WelcomeEmp($user);
        return Base::touser('Employee Created', true);
    }
    else
    {
        return Base::touser('Employee Limit Crossed not able to create', false);
    }
    }

    public function show(Request $request,$id)
    {
        try {
            if ($this->admin || $this->backend) {


            if(!empty($request->input('belongs')))
            {

              $data = User::with('role')->with('cust')->find($id)->toArray();

             }
             else
             {
                  $data = User::with('role')->find($id)->toArray();
             }





                $data['role'] = $data['role']['display_name'];

                return Base::touser($data, true);

            } elseif ($this->manager) {

                if(!empty($request->input('belongs')))
            {


                $data = User::with('role')->with('cust')->where('belongs_manager', $this->emp_id)

                    ->where('user_id', $id)->get()->toArray()[0];

             }
             else
             {

                $data = User::with('role')->where('belongs_manager', $this->emp_id)

                    ->where('user_id', $id)->get()->toArray()[0];
             }





                $data['role'] = $data['role']['display_name'];

                $query = task::query();
                $query->where('emp_id',$id);

                $query->whereIn('status',array('Started Ride','In Supplier Place','Products Picked up'));
                $alivedata = $query->get();
                if(count($alivedata) == 0){
                    $data['alive'] = false;
                }else{
                    $data['alive'] = true;                }
                return Base::touser($data, true);

            } else {
                return Base::throwerror();
            }
        } catch (\Exception $e) {
            return Base::throwerror();
        }
    }

    public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'role_id'    => 'required',
            'first_name' => 'required',
            'last_name'  => 'required',
             'street'        => 'required',
            // 'state'         => 'required',
           // 'zipcode'       => 'required',
            //             'country'       => 'required',
            'phone'      => 'required',

            // 'profile_image' => 'required',
            'email'      => 'required|email|unique:user,email,' . $id . ',user_id',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $user             = new User();
        $user             = $user->where('user_id', '=', $id)->first();
        $user->role_id    = $data['role_id'];
        $user->first_name = $data['first_name'];
        $user->last_name  = $data['last_name'];

        $user->user_pwd = isset($data['user_pwd']) ? $data['user_pwd'] : $user->user_pwd;
        $user->phone    = $data['phone'];
        $user->email    = $data['email'];
        $user->comments1 = $request->get("comments1");
        $user->comments2  = $request->get("comments2");
        $user->comments3    = $request->get("comments3");
        $user->comments4 = $request->get("comments4");
        $user->comments5  = $request->get("comments5");
        $user->whatsapp = $request->get("whatsapp");
        $user->resaddress  = $request->get("resaddress");
        $user->is_delete      = $data['active'];
        $user->city    = isset($data['city']) ? $data['city'] : null;
        $user->street  = isset($data['street']) ? $data['street'] : null;
        $user->state   = isset($data['state']) ? $data['state'] : null;
         $user->zipcode = isset($data['zipcode']) ? $data['zipcode'] : 0;
        $user->country = isset($data['country']) ? $data['country'] : null;

        $user->profile_image = isset($data['profile_image']) ? json_encode($data['profile_image'], true) : '[]';
        $user->phone_imei    = isset($data['phone_imei']) ? $data['phone_imei'] : '';



        $user->is_active     = isset($data['is_active']) ? $data['is_active'] : 0;



        if ($data['role_id'] == 1) {

            if ($this->admin || $this->backend) {

                if (empty($data['belongs_manager'])) {

                    return Base::touser('Sale Person must have belongs to manager');
                }

                $user->belongs_manager = isset($data['belongs_manager']) ? $data['belongs_manager'] : null;
            } elseif ($this->manager) {

                $user->belongs_manager = $this->emp_id;
            } else {
            }
        }

        $user->save();


        if($user->is_active  == 0 && $data['role_id'] == 1)
        {


            $customers = new Customers;

            $customers->where('emp_id',$user->user_id)->update(array('emp_id' => $user->belongs_manager));


        }
        
            $restrictcontroller = new restrictcontroller();
         $restrictcontroller->updatecount();
        return Base::touser('Employee Updated', true);
    }

    public function destroy($id)
    {

        try {

            $api = new User();
            $api = $api->find($id);
            $api->delete();
            return Base::touser('Employee Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Employee its connected to Other Data !");
            //return Base::throwerror();
        }

    }

    public function recover(Request $request)
    {
        $api  = new User();
        $id   = $request->input('id');
        $user = $api->onlyTrashed()->where('user_id', '=', $id)->first();
        $user->restore();
        return Base::touser('Employee Recovered', true);
    }
}
