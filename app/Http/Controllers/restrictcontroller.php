<?php
namespace App\Http\Controllers;
use App\Models\timezone;
use App\Models\distance;
use App\Models\timezone as timezonemang;
use App\Models\packageinfo;
use App\Http\Controllers\Base;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Toin0u\Geotools\Facade\Geotools;
use Validator;
use \DateTime;
use \DateTimeZone;
use App\Models\User;
use App\Models\EmpCustSchedule as task;
use App\Models\EmpSchedule as allocation;
use App\Models\ScheduleTaskStatus;
use App\Models\Review;
use App\Models\TravelHistory as api;
use App\Models\Customer;
use App\Models\UserPackage;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;






/**
* 
*/
class restrictcontroller extends Controller
{

	public function clearbase(){
      $ids = ['2','3','4','5','6','7','8','9','10','11','12','13','14','22','23','24','26','27','29','30','32','33','34','36','39','43','44','45','47','52','54','56','58','62','63','79','82','88','91','110','113','115','118','119','120','122','123','124','125','126','127','128','129','135','136','137','138','139','140','141','142','143','144','145','146','147','148','149','150','153','154','155','156','157','158','159','160','163','164','165','166','167','168','169','170','171','172','173','174','175','176','177','178','179','180','181','183','188','193','194','195','196','197','198','199','200','201','202','203','204','205','206','207','208','209','210','211','212','214','215','216','217','218','219','220','221','238','239','253','255','256','259','260','261','262','263'];
      $i = 0;
      for(;$i<count($ids);$i++){
      		$allocation =  allocation::where('emp',$ids[$i])->get();
      		$task_ids = [];

      		for($j=0;$j<count($allocation);$j++){
      		$task_ids [$j] = $allocation[$j]['task_id'];
      	}	if(count($task_ids)!= 0){
      		 ScheduleTaskStatus::where('emp_id',$ids[$i])->delete();
      		 allocation::where('emp',$ids[$i])->delete();
      	     Review::whereIn('task_id',$task_ids)->delete();
      	     api::whereIn('user_id',$ids[$i])->delete();
      		 task::whereIn('id',$task_ids)->delete();

      	}

      } 
             ScheduleTaskStatus::whereIn('emp_id',$ids)->delete();
      		 allocation::whereIn('emp',$ids)->delete();
      	     Review::whereIn('emp_id',$ids)->delete();
      	     api::whereIn('user_id',$ids)->delete();
      	     task::where('emp_id',$ids)->delete();

             $cusdata=Customer::whereIn('emp_id',$ids)->get();
             $cust_ids = [];
             	for($v=0;$v<count($cusdata);$v++){
      		$cust_ids[$v] = $cusdata[$v]['id'];
        	}
        	$tasksdata = task::whereIn('cust_id',$cust_ids)->get();
        	$tasks = [];
	       for($a=0;$a<count($tasksdata);$a++){
      		$tasks[$a] = $tasksdata[$a]['id'];
        	}
        	 allocation::whereIn('task_id',$tasks)->delete();
        	 ScheduleTaskStatus::whereIn('task_id',$tasks)->delete();
        	 Review::whereIn('task_id',$tasks)->delete();
      	     task::whereIn('cust_id',$cust_ids)->delete();
             Customer::whereIn('emp_id',$ids)->delete();
             User::whereIn('user_id',$ids)->delete();

	}
	public function getTimezone(){
		$timezone =  timezone::all();
		return $timezone;
// $timezones = array(
// 'Pacific/Midway'       => "(GMT-11:00) Midway Island",
//     'US/Samoa'             => "(GMT-11:00) Samoa",
//     'US/Hawaii'            => "(GMT-10:00) Hawaii",
//     'US/Alaska'            => "(GMT-09:00) Alaska",
//     'US/Pacific'           => "(GMT-08:00) Pacific Time (US &amp; Canada)",
//     'America/Tijuana'      => "(GMT-08:00) Tijuana",
//     'US/Arizona'           => "(GMT-07:00) Arizona",
//     'US/Mountain'          => "(GMT-07:00) Mountain Time (US &amp; Canada)",
//     'America/Chihuahua'    => "(GMT-07:00) Chihuahua",
//     'America/Mazatlan'     => "(GMT-07:00) Mazatlan",
//     'America/Mexico_City'  => "(GMT-06:00) Mexico City",
//     'America/Monterrey'    => "(GMT-06:00) Monterrey",
//     'Canada/Saskatchewan'  => "(GMT-06:00) Saskatchewan",
//     'US/Central'           => "(GMT-06:00) Central Time (US &amp; Canada)",
//     'US/Eastern'           => "(GMT-05:00) Eastern Time (US &amp; Canada)",
//     'US/East-Indiana'      => "(GMT-05:00) Indiana (East)",
//     'America/Bogota'       => "(GMT-05:00) Bogota",
//     'America/Lima'         => "(GMT-05:00) Lima",
//     'America/Caracas'      => "(GMT-04:30) Caracas",
//     'Canada/Atlantic'      => "(GMT-04:00) Atlantic Time (Canada)",
//     'America/La_Paz'       => "(GMT-04:00) La Paz",
//     'America/Santiago'     => "(GMT-04:00) Santiago",
//     'Canada/Newfoundland'  => "(GMT-03:30) Newfoundland",
//     'America/Buenos_Aires' => "(GMT-03:00) Buenos Aires",
//     'Greenland'            => "(GMT-03:00) Greenland",
//     'Atlantic/Stanley'     => "(GMT-02:00) Stanley",
//     'Atlantic/Azores'      => "(GMT-01:00) Azores",
//     'Atlantic/Cape_Verde'  => "(GMT-01:00) Cape Verde Is.",
//     'Africa/Casablanca'    => "(GMT) Casablanca",
//     'Europe/Dublin'        => "(GMT) Dublin",
//     'Europe/Lisbon'        => "(GMT) Lisbon",
//     'Europe/London'        => "(GMT) London",
//     'Africa/Monrovia'      => "(GMT) Monrovia",
//     'Europe/Amsterdam'     => "(GMT+01:00) Amsterdam",
//     'Europe/Belgrade'      => "(GMT+01:00) Belgrade",
//     'Europe/Berlin'        => "(GMT+01:00) Berlin",
//     'Europe/Bratislava'    => "(GMT+01:00) Bratislava",
//     'Europe/Brussels'      => "(GMT+01:00) Brussels",
//     'Europe/Budapest'      => "(GMT+01:00) Budapest",
//     'Europe/Copenhagen'    => "(GMT+01:00) Copenhagen",
//     'Europe/Ljubljana'     => "(GMT+01:00) Ljubljana",
//     'Europe/Madrid'        => "(GMT+01:00) Madrid",
//     'Europe/Paris'         => "(GMT+01:00) Paris",
//     'Europe/Prague'        => "(GMT+01:00) Prague",
//     'Europe/Rome'          => "(GMT+01:00) Rome",
//     'Europe/Sarajevo'      => "(GMT+01:00) Sarajevo",
//     'Europe/Skopje'        => "(GMT+01:00) Skopje",
//     'Europe/Stockholm'     => "(GMT+01:00) Stockholm",
//     'Europe/Vienna'        => "(GMT+01:00) Vienna",
//     'Europe/Warsaw'        => "(GMT+01:00) Warsaw",
//     'Europe/Zagreb'        => "(GMT+01:00) Zagreb",
//     'Europe/Athens'        => "(GMT+02:00) Athens",
//     'Europe/Bucharest'     => "(GMT+02:00) Bucharest",
//     'Africa/Cairo'         => "(GMT+02:00) Cairo",
//     'Africa/Harare'        => "(GMT+02:00) Harare",
//     'Europe/Helsinki'      => "(GMT+02:00) Helsinki",
//     'Europe/Istanbul'      => "(GMT+02:00) Istanbul",
//     'Asia/Jerusalem'       => "(GMT+02:00) Jerusalem",
//     'Europe/Kiev'          => "(GMT+02:00) Kyiv",
//     'Europe/Minsk'         => "(GMT+02:00) Minsk",
//     'Europe/Riga'          => "(GMT+02:00) Riga",
//     'Europe/Sofia'         => "(GMT+02:00) Sofia",
//     'Europe/Tallinn'       => "(GMT+02:00) Tallinn",
//     'Europe/Vilnius'       => "(GMT+02:00) Vilnius",
//     'Asia/Baghdad'         => "(GMT+03:00) Baghdad",
//     'Asia/Kuwait'          => "(GMT+03:00) Kuwait",
//     'Africa/Nairobi'       => "(GMT+03:00) Nairobi",
//     'Asia/Riyadh'          => "(GMT+03:00) Riyadh",
//     'Europe/Moscow'        => "(GMT+03:00) Moscow",
//     'Asia/Tehran'          => "(GMT+03:30) Tehran",
//     'Asia/Baku'            => "(GMT+04:00) Baku",
//     'Europe/Volgograd'     => "(GMT+04:00) Volgograd",
//     'Asia/Muscat'          => "(GMT+04:00) Muscat",
//     'Asia/Tbilisi'         => "(GMT+04:00) Tbilisi",
//     'Asia/Yerevan'         => "(GMT+04:00) Yerevan",
//     'Asia/Kabul'           => "(GMT+04:30) Kabul",
//     'Asia/Karachi'         => "(GMT+05:00) Karachi",
//     'Asia/Tashkent'        => "(GMT+05:00) Tashkent",
//     'Asia/Kolkata'         => "(GMT+05:30) Kolkata",
//     'Asia/Kathmandu'       => "(GMT+05:45) Kathmandu",
//     'Asia/Yekaterinburg'   => "(GMT+06:00) Ekaterinburg",
//     'Asia/Almaty'          => "(GMT+06:00) Almaty",
//     'Asia/Dhaka'           => "(GMT+06:00) Dhaka",
//     'Asia/Novosibirsk'     => "(GMT+07:00) Novosibirsk",
//     'Asia/Bangkok'         => "(GMT+07:00) Bangkok",
//     'Asia/Jakarta'         => "(GMT+07:00) Jakarta",
//     'Asia/Krasnoyarsk'     => "(GMT+08:00) Krasnoyarsk",
//     'Asia/Chongqing'       => "(GMT+08:00) Chongqing",
//     'Asia/Hong_Kong'       => "(GMT+08:00) Hong Kong",
//     'Asia/Kuala_Lumpur'    => "(GMT+08:00) Kuala Lumpur",
//     'Australia/Perth'      => "(GMT+08:00) Perth",
//     'Asia/Singapore'       => "(GMT+08:00) Singapore",
//     'Asia/Taipei'          => "(GMT+08:00) Taipei",
//     'Asia/Ulaanbaatar'     => "(GMT+08:00) Ulaan Bataar",
//     'Asia/Urumqi'          => "(GMT+08:00) Urumqi",
//     'Asia/Irkutsk'         => "(GMT+09:00) Irkutsk",
//     'Asia/Seoul'           => "(GMT+09:00) Seoul",
//     'Asia/Tokyo'           => "(GMT+09:00) Tokyo",
//     'Australia/Adelaide'   => "(GMT+09:30) Adelaide",
//     'Australia/Darwin'     => "(GMT+09:30) Darwin",
//     'Asia/Yakutsk'         => "(GMT+10:00) Yakutsk",
//     'Australia/Brisbane'   => "(GMT+10:00) Brisbane",
//     'Australia/Canberra'   => "(GMT+10:00) Canberra",
//     'Pacific/Guam'         => "(GMT+10:00) Guam",
//     'Australia/Hobart'     => "(GMT+10:00) Hobart",
//     'Australia/Melbourne'  => "(GMT+10:00) Melbourne",
//     'Pacific/Port_Moresby' => "(GMT+10:00) Port Moresby",
//     'Australia/Sydney'     => "(GMT+10:00) Sydney",
//     'Asia/Vladivostok'     => "(GMT+11:00) Vladivostok",
//     'Asia/Magadan'         => "(GMT+12:00) Magadan",
//     'Pacific/Auckland'     => "(GMT+12:00) Auckland",
//     'Pacific/Fiji'         => "(GMT+12:00) Fiji",
//     );
			
// 			$keys=array_keys($timezones);
// 			$k=0;
// 			foreach ($timezones as $zone) {
// 					$timezone = new timezone();
// 					         $timezone->id=$k;
//                              $timezone->desc=$keys[$k];
//                              $timezone->timezone=$zone;
//                              $timezone->name=$zone;
//                              $timezone->save();
// $k++;
            
// 		}
	}
	public function updatebasicinfo(Request $request){
		$data= $request->get('data');
		try{
			  $timedata = timezonemang::where('desc',$data['timezone'])->get(['timezone']);
$zonename = $timedata[0]['timezone'];
			$User = User::where('user_id',$this->emp_id)->first();
		           $User->timezone=$data['timezone'];
					$User->mailnote=$data['mailnote'];
					$User->smsnote=$data['smsnote'];
				    $User->street=$data['street'];
            $User->is_multipick=$data['is_multipick'];
				    $User->timezonename = $zonename;



		$User->update();
		return Base::touser($User, true);
	}catch(Exception $e){
		
	}
	}
	public function getGbsData(Request $request){
		try{
			$data = $request->get('data');
		   $start = Base::tomysqldatetime($data['start_date']) ;
           $end   = Base::tomysqldatetime($data['end_date']);
          

      $query = distance::query();
      if($data['emp_id']!= 0){
      	$emp=$data['emp_id'];
       $query->where('emp_id',$emp)
             ->where('start_time', '<=',$end)
             ->where('start_time','>=',$start);
             $data = $query->get();
       return Base::touser($data, true);
   }else {
       $query->where('start_time', '<=',$end)
             ->where('start_time','>=',$start);
            $data = $query->get();
       return Base::touser($data, true);        }
      

	}catch(Exception $e){
		return "No Data found";
	}
	}
public function milagereport(Request $request){
		try{
			$data = $request->get('data');
		   $start = $data['order_id'] ;
          

      $query = distance::query();
      if($data['order_id']){
       $query->where('order_id',$start);
             $data = $query->get();
       return Base::touser($data, true);
   }
      

	}catch(Exception $e){
		return "No Data found";
	}
	}
public function checkusers(){
try{
  // $totalcount = User::where('is_delete','false')->where('belongs_manager',$this->emp_id)->count();
  $userPackage = UserPackage::where('user_id',$this->emp_id)->with('packageinfo')->get();
  return $userPackage;
}catch(Exception $e){
  return "No Data found";
}
}
public function updatecount(){
  try{
      $totalcount = User::where('is_delete','false')->where('belongs_manager',$this->emp_id)->count();
      $emps = UserPackage::where('user_id',$this->emp_id)->first();
      $packageinfo = packageinfo::where('id',$emps['package_id'])->first();
      $packageinfo['no_of_emp'];
   // $emps->no_of_emp = $emps['no_of_emp'] - $totalcount;
   $emps->save();
   return $emps;
  }catch(Exception $e){
      return "No Data found";    
  }
}
// public function packagemode(){
//   $users = User::all();
//   foreach ($users as $user => $value) {
//     $userpack = new UserPackage();
//     $userpack->package_id;
//     $userpack->beg_date;
//     $userpack->end_date;
//     $userpack->is_trail;
//     $userpack->price
//     $userpack->no_of_emp;
//     $userpack->no_of_cust;
//     $userpack->no_of_task;


//   }
  
// }
public function  getOrderData(){
$client = new Client();
$store = $client->request('GET','https://www.localisbig.com/api/1/entity/ms.store_locations',[
  'headers'        => ['access-key' => 'b9696cb420a19dd3f59ddbd5bd212d61'],
]);
$storedata =  $store->getBody();
$stdata = json_decode($storedata)->data;


$res = $client->request('GET', 'https://www.localisbig.com/api/1/entity/ms.orders?sort=[{"field":"updated_on",order : 1}]', [
    'headers'        => ['access-key' => 'b9696cb420a19dd3f59ddbd5bd212d61'],

]);
if($res->getStatusCode() == 200){
$data =  $res->getBody(); // { "type": "User", ....
//echo $data;
$order_datas = json_decode($data)->data;
echo  count($order_datas);
foreach ($order_datas as $newdata => $order_data) {
  # code...
$receiver_address = $order_data->shipping_address;
$receiver_phone = $receiver_address->phone;
$receiver_name = $receiver_address->full_name;
$receiver_address = $receiver_address->address.$receiver_address->city.$receiver_address->state;
//print_r($receiver_address);

$pickup_point = $order_data->seller_name;
$items = $order_data->items;
$order_id = $order_data->order_id;
$notes = '';
$cmt = '';
$mob = $order_data->payment_method->type;
if(isset($receiver_email -> $order_data->email)){
  $receiver_email = $order_data->email;
}else{
  $receiver_email = '';
}

foreach ($order_data->items as $item => $dataitem) {
  # code...
  if($notes != ''){
    $notes = $notes.','.$dataitem->name;
  }else{
    $notes = $dataitem->name;
  }
  
}
echo count($order_data->items);
if(isset($order_data->options->select_delivery_time)){
  $len = strlen($order_data->options->select_delivery_time);
  $strtime = substr($order_data->options->select_delivery_time,($len - 8));
  $curtime = strtotime('today'.$strtime);
  $delivery_date = date('Y-m-d H:i:s', $curtime);
}elseif (isset($order_data->options->select_delivery)) {
  $len = strlen($order_data->options->select_delivery);
  $strtime = substr($order_data->options->select_delivery,($len - 8));
  $curtime = strtotime('today'.$strtime);
  $delivery_date = date('Y-m-d H:i:s', $curtime);
}  
$pickup_add = '';
$pickup_ladd = '';
$pickup_long = '';
  $newdata = json_encode($order_data->shipping_by_vendor);
  $ventors = json_decode($newdata,true);
  foreach ($ventors as $vent => $ventor) {
    try{
      if(isset($order_data->available_shipping_charges)){
        $strdata = $order_data->available_shipping_charges->$vent;
      }
    }
    //return $vent;
    catch(Exception $e){
            return $order_data->available_shipping_charges;
      //return $strdata;
    }
foreach ($stdata as $st => $stored) {
        if($stored->user_id == $strdata[0]->seller){
            if($stored->address != ''){
             if(isset($stored->latitude)){
              if($pickup_add != ''){
                $pickup_add = $pickup_add."||,". $stored->address;
              $pickup_ladd = $pickup_ladd.",".$stored->latitude;
              $pickup_long = $pickup_long.",".$stored->longitude;
               
              }else{
               $pickup_add = $stored->address;
              $pickup_ladd = $stored->latitude;
              $pickup_long = $stored->longitude;
                
              }
              // echo $pickup_ladd;
             }
               
            }else{
              echo "string";
            }
            
        }
    } 
  };

  if(isset($order_data->client_details->geo_location->ll[0])){
$lat = $order_data->client_details->geo_location->ll[0];
$long = $order_data->client_details->geo_location->ll[1];
}else{
  $lat = '';
  $long = '';
}
  $orders= task::where('order_id',$order_id)->get();
  if(count($orders)){
   try{
             $task                     = new task();
        $task->schedule_date_time = $delivery_date;        
            $Customer             = new Customer();
            $Customer->name       = $receiver_name;
            $Customer->email      = $receiver_email;
            $Customer->emp_id     = 1;
            $Customer->address    = $receiver_address;
            $Customer->loc_lat    = $lat;
            $Customer->loc_lng    = $long;
            $Customer->contact_no = $receiver_phone;
            $Customer->save(); 
        $task->cust_id        = $Customer->id;
        $task->notes          = $notes;
        $task->pick_address   = $pickup_add;
        $task->order_id       = $order_id;
        $task->comments       = $cmt;
        $task->mob            = $mob;
        $task->added_by       = 1;
        $task->cust_email     = $receiver_email;
        $task->sender_name    = '';
        $task->sender_number  = '';
        $task->sender_number  = '';
        $task->picktime       = Base::tomysqldatetime(date("Y-m-d H:i:s"));
        $task->pickup_long    = $pickup_ladd;
        $task->pickup_ladd    = $pickup_long;
        $task->sent_address   = '';
        $task->status         = 'Unallocated';
        $task->method         = 'pickup';
        $task->is_new_address = true;
        
            $task->cust_phone   = $receiver_phone;
            $task->loc_lat      = $lat;
            $task->loc_lng      = $long;
            $task->cust_address = $receiver_address;
    
       
        $task->save();
        
        $task_status             = new ScheduleTaskStatus();
        $task_status->emp_id     = 1;
        $task_status->task_id    = $task->id;
        $task_status->address    = '';
        $task_status->lat        = '';
        $task_status->long       = '';
        $task_status->status     = 'Unallocated';
        $task_status->timestamps = Base::tomysqldatetime(date("Y-m-d H:i:s"));
        $task_status->save();  

   }catch(Exception $e){

   } 
  }
}
}

}
public function existaddemp(){
  $users=User::where('role_id','2')->get();

  foreach ($users as $use => $user) {
          $userPackage = new UserPackage();

  $userPackage->user_id=$user->user_id;
          $userPackage->package_id = "1";
         $date = date('Y-m-d H:i:s');
                 // $d=strtotime("+1 Months"); 
         $d=strtotime("+13 days");
                 $enddate = date("Y-m-d H:i:s", $d);
                 $userPackage->beg_date = $date;
                 $userPackage->end_date = $enddate;
          // $userPackage->no_of_emp = 2;
          $userPackage->no_of_cust= 0;
          $userPackage->no_of_task = 0;
          $userPackage->save();
          $user = User::where('user_id',$user->user_id)->first();
          $user->current_package_id = $userPackage->id;
          $user->update();

}
# code...
  }

}

?>